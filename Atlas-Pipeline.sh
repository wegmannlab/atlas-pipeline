#!/bin/bash
#SBATCH --err log/snakemake-%A.out
#SBATCH --out log/snakemake-%A.out
#SBATCH --mem=2G
#SBATCH --time=100:00:00
#SBATCH --job-name=ATLAS-Pipeline
#SBATCH --partition=pibu_el8

#########################################################################################################
#--------------------------this script executes the ATLAS-pipeline--------------------------------------#
#########################################################################################################

#########################################################################################################
#########################################################################################################
# Author           : Ilektra Schulz
#                    ilektra.schulz@unifr.ch
# Source           : https://bitbucket.org/wegmannlab/atlas-pipeline/
#
# Usage            : Execute with bash run_ATLAS-pipeline.sh within atlas-pipeline folder
#
# Required files   : config file containing all paths and thresholds for your analysis (see example files)
#                    cluster.json for SLURM cluster specifications (see example.cluster.json)
#                    samples.tsv with paths to your inputfiles (see example.samples.tsv)
#
# Date             : 2022-07-27
#########################################################################################################
#########################################################################################################

#getopt needs a date set
date=$(date -I)

#unset parameters
unset usage dryrun config cluster clusterfile pallas plot jobs snakeParam

#########################################################################################################
#-----getopt
#########################################################################################################
# define the help

usage='
\n \******************************ATLAS-Pipeline******************************************\
\n\t Author:\t Ilektra Schulz
\n\t Contact:\t ilektra.schulz@unifr.ch ; daniel.wegmann@unifr.ch
\n\t    
\n\t    Default parameters are specified for low-depth and ancient DNA analysis
\n\t    Usage: bash Atlas-Pipeline.sh <options>
\n
\n\t    Before running the pipeline:
\n\t    To ensure package continuity, we advise the use of the provided conda environment.
\n\t    Prepare a config-file as described in the documentation (https://bitbucket.org/wegmannlab/atlas-pipeline/wiki/Home)
\n\t
\n \**************************************************************************************\
\n
\n\t -h \t  open help page
\n\t\t  
\n\t -d \t  perform a dry-run of the pipeline
\n\t\t  
\n\t -f \t  specify your config-file (required)
\n\t\t      copy the example config-file to a desired location and change the parameters within
\n\t\t      attention!: do not alter the example.config.yaml file!
\n\t\t                  in case of an update of the pipeline, it will be overwritten!
\n\t\t
\n\t -p \t  make a plot of the DAG structure
\n\t\t
\n\t -c \t  run on a slurm cluster
\n\t\t\t    cluster file can be provided. Default: cluster.json
\n\t\t      
\n\t -j \t  specify how many jobs should be submitted alongside (only on cluster system). Default: 50
\n\t\t
\n\t -s \t  pass a snakemake command in quotes to the snakemake command-line (e.g. "--until rulename")
\n\t\t
\n\t\t
\n \\\ \t\t\t\t\t\t\t\t\t\t \\\
\n \\\ \t Disclaimer: \t\t\t\t\t\t\t\t\t \\\
\n \\\ \t No guarantee for the content and results of this script to be bug-free. \\\
\n
\n \**************************************************************************************\
'
#get options to variables
while getopts ":hdf:c:pj:s:" OPTION
do
    case $OPTION in
    h)
        echo -e $usage
        exit 1
        ;;
    d)
        echo -e "performing dry-run of the pipeline \n"
        dryrun="yes"
        exec="no"
        ;;
    f)
        config=${OPTARG}
        if [[ -z $config ]]; then
            echo -e $usage
            echo -e "ERROR: you must specify a config file with the option -f !!"
            exit 1
        fi
        if [[ -f $config ]]; then
            echo -e "found config file: ${config} \n"
        else
            echo -e "ERROR: config-file not found!"
            exit 1
        fi
        ;;
    c)
        cluster="yes"
        if test "$OPTARG" = "$(eval echo '$'$((OPTIND - 1)))"; then
                clusterfile="$OPTARG" 
                echo "performing analysis on cluster with parameters from $clusterfile" 
            else
                OPTIND=$((OPTIND - 1))
                clusterfile=cluster.json
                echo "performing analysis on cluster with default $clusterfile ."
        fi 
        ;;
    p)      
        echo -e "plotting DAG in images \n"
        plot="yes"
        ;;
    j)
        jobs=${OPTARG}
        echo -e "executing $jobs jobs in parallel (option -j) \nonly applied on cluster systems (option -c)"
        ;;
    s)
        snakeParam=${OPTARG}
        echo -e "snakemake will be executed with the following additional parameter(s): $snakeParam"
        ;;
    \?)
        echo -e "ERROR: no valid parameters defined! \n\n"
        echo -e $usage
        exit 1
        ;;
    :)
        case "$OPTARG" in
            c)
                clusterfile=cluster.json
                cluster="yes"
                echo "performing analysis on cluster with default $clusterfile .."
                ;;
            *) 
                echo "option requires an argument -- $OPTARG" 
                ;;
        esac 
        ;;
    esac
done 

#detect mode

mode=$(awk '$1 == "runScript:" {print $2}' $config )
echo "mode: $mode"

case $mode in 
	Gaia)
		echo "you are executing the Gaia pipeline (runScript: $mode)"
		#check for indexed reference (or maybe within snakemake?)
		#check for no spaces in input table
	  ;;
	Rhea)
		echo "you are executing the Rhea target creator pipeline (runScript: $mode)"
    ;;
  Perses | Pallas)
    mkdir -p readgroups
    ;;
  Tests)
    echo "creating testset"
    ;;
  *)
    echo -e "unknown parameter in configfile runScript: " $mode
    exit 1
    ;;
esac

#################################################################################################
#-----for ATLAS-pipeline
#################################################################################################

baseSN="snakemake -s Snakefile --configfile $config --rerun-incomplete"
#unlock history
echo -e "unlocking snakemake..."
$baseSN --unlock $snakeParam
snakemake -s Snakefile --configfile $config --unlock --rerun-incomplete $snakeParam

# make plots
if [[ $plot == "yes" ]]; then
    $baseSN --dag | dot -Tpdf > plots/pipeline-${mode}-${date}.pdf
fi

#dryrun
if [[ $dryrun == "yes" ]]; then 
    $baseSN -np $snakeParam
else
  # re-direct temporary output to desired directory if tmpDir is present in config file.
    addTemp=$(awk -v q="'" '{if ($1=="tmpDir:") {tmp=$2; print "--default-resources tmpdir="q tmp q; exit}}' $config)
    baseSN=$(echo $baseSN $addTemp)
    #cluster
    if [[ $cluster == "yes" ]]; then
        #define job number default
        if [ -z $jobs ]; then jobs=50; echo "Executing $jobs jobs in parallel (default)" ; fi

        #check for clusterfile
        if [[ ! -f $clusterfile ]]; then
                echo -e "ERROR: no cluster file present! Looking for $clusterfile."
                exit 1
        else
            clusterConf=$(jq '.__default__ | keys' $clusterfile | tr "[" " " | tr "]" " " | tr "," " " | tr '"' ' ' )
            clusterTmp=$(for i in $clusterConf ; do echo "--${i} {cluster.${i}}"; done | tr '\n' ' ')
            clusterIn=$(echo  " sbatch $clusterTmp ")
            $baseSN --show-failed-logs --latency-wait 30 -j $jobs --use-conda --cluster-config $clusterfile --cluster " ${clusterIn} --parsable" $snakeParam
            #--cluster-status scripts/status.py
        fi
    #front-machine
    else
        tag=$(date +"%T"| sed 's/:/-/g')
        #using one core only
        $baseSN -c1 --use-conda $snakeParam 2>&1 | tee log/snakemake-${date}.txt
    fi
fi

#########################################################################################################
#########################################################################################################
