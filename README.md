# README for a flexible aDNA-pipeline using Atlas and Snakemake


For information how to run the pipeline, consult the [ATLAS-Pipeline's wiki](https://atlaswiki.netlify.app/atlas-pipeline). 

>                          ___
>                            {-)   |\
>                       [m,].-"-.   /
>      [][__][__]         \(/\__/\)/
>      [__][__][__][__]~~~~  |  |
>      [][__][__][__][__][] /   |
>      [__][__][__][__][__]| /| |
>      [][__][__][__][__][]| || |  ~ ~ ~  
>      [__][__][__][__][__]__,__,  \__/  
> ejm

