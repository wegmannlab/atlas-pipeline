
numSamples=5

rule createTestset:
    input:
        ["tests/ind"+str(i)+".txt" for i in range(1,numSamples+1)]
    output:
        bams=[OUTPATH +"/0.Testset/ATLAS_simulations_ind"+str(i)+".bam" for i in range(1,numSamples)],
        ref=(OUTPATH +"/0.Testset/ATLAS_simulations.fasta"),
        fqgs= [OUTPATH+"/0.Testset/ATLAS_simulations_ind"+str(i)+"."+rg+"_R1_001.fastq.gz" for i in range(1,numSamples+1) for rg in ["RG1","RG2"]]
    params:
        prefix=(OUTPATH +"/0.Testset/ATLAS_simulations"),
        sampleSize=numSamples
    conda:
        "../environments/environment_bamUtil.yaml"
    shell:
        """
        {config[atlas]} simulate --type HW --sampleSize {params.sampleSize} --chrLength 10000,5000,1000 --RGInfo $(echo {input}| tr '[:blank:]' ',') --depth 50 --out {params.prefix} --writeVariantBED --fragmentLength "normal(100,10)[0,150]"
        for i in {params.prefix}_ind*bam; do bam bam2fastq --in $i --splitRG --outBase ${{i/.bam/}} --gzip; 
        for rg in RG1 RG2; do mv ${{i/.bam/}}.${{rg}}.fastq.gz ${{i/.bam/}}.${{rg}}_R1_001.fastq.gz ; done; done
        """

rule BWAindex:
    input: (OUTPATH +"/0.Testset/ATLAS_simulations.fasta")
    output: (OUTPATH +"/0.Testset/ATLAS_simulations.fasta.bwt")
    conda:
        "../environments/environment_align.yaml"
    shell: "bwa index {input}"
