#########################################################################################################
#########################################################################################################
# Author           : Ilektra Schulz
#                    ilektra.schulz@unifr.ch
# Source           : https://bitbucket.org/wegmannlab/atlas-pipeline/
#########################################################################################################
#########################################################################################################
import pysam

# take first BAM file to get contig number and lengths)
bamFile = pysam.AlignmentFile(df.Path[0]+df.index[0]+".bam", mode='rb')
contigNames = list(bamFile.references)
contigLengths = list(bamFile.lengths)

if PARA == "T":
    # check if not all contigs should be analyzed
    if CHRPAR != "all":
        chlist=CHRPAR.split(",")
        cleng=[]
        for i in range(len(contigNames)):
            if contigNames[i] in chlist:
                cleng.append(contigLengths[i])
        contigNames = chlist
        contigLengths = cleng
    numContigs=len(contigLengths)

    #if no binning shoult happen, binNr = chrNr
    if BINCOUNT == "F":
        binDict = {}
        binList = range(1,(numContigs +1))
        for i in binList:
            binDict[i] = [contigNames[i-1]]
    else:
        numBins = BINCOUNT
        if numBins > numContigs:
            print("Error: number of bins cannot be higher than number of contigs! \n", numBins , ">", numContigs)
            raise        
        maxBins = numBins
        binList = range(1,(maxBins+1))
        
        #set starting points
        restLen = sum(contigLengths)
        binLen = (restLen/numBins)
        currLen = 0
        currBin = 1
        binVec = []
        binDict = {}
        for i in range(1,(numBins+1)):
            binDict[i] = []
        for i in range(numContigs):
            if currBin == maxBins:
                if (i+1) == len(contigLengths):
                    binDict[currBin].append(contigNames[i])
                    binVec.append(currLen)
                else:
                    binDict[currBin].append(contigNames[i])
                    currLen = currLen + contigLengths[i]
            else:
                newLen = currLen + contigLengths[i]
                if newLen > binLen:
                    #get distances
                    distSmall = (binLen - currLen)
                    distBig = (newLen - binLen)
                    #check weather or not to keep latest contig in bin
                    if (contigLengths[i] >= binLen) or (distBig < (distSmall/numBins)):
                        #latest contig stays in this bin
                        binVec.append(newLen)
                        restLen = ( restLen - newLen )
                        numBins = ( numBins - 1 )
                        binLen = ( restLen / numBins )
                        currLen=0
                        binDict[currBin].append(contigNames[i])
                        currBin = ( currBin + 1 )
                    else:
                        #go to next bin, and take latest contig with you
                        binVec.append(currLen)
                        restLen = ( restLen - currLen )
                        numBins = (numBins - 1 )
                        binLen = ( restLen / numBins )
                        currLen = contigLengths[i]
                        currBin = ( currBin + 1 )
                        binDict[currBin].append(contigNames[i])
                else:
                    #append
                    binDict[currBin].append(contigNames[i])
                    currLen = newLen

#########################################################################################################
#########################################################################################################
