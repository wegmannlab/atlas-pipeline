
#########################################################################################################
#-----------------------------------------------GAIA----------------------------------------------------#
#########################################################################################################
# Author           : Ilektra Schulz
#                    ilektra.schulz@unifr.ch
# Source           : https://bitbucket.org/wegmannlab/atlas-pipeline/
#########################################################################################################
#########################################################################################################

#defaults
MAXTHREADS=config.get("threads",1)
JAVAMEM=config.get("javaMem","10G")
#trim
ADAPTERS=config.get("adapter","T")
ADAP1=config.get("adapterSequence1","standardAdapters")
ADAP2=config.get("adapterSequence2","standardAdapters")
MINLEN=config.get("minLen","30")
MINQUAL=config.get("minQ","0")


"""
if REFLOC == 1:
    BWAIDX = REF
    PICARDDICT = REF
elif REFLOC == 2:
    BWAIDX = config['bwaIndex']
    PICARDDICT = config['picardDict']
elif REFLOC ==3:
    BWAIDX = "exec"
    PICARDDICT = "exec"
"""
#align
ALIGNER=config.get("aligner", "BWAmem")
BWACREATEINDEX=config.get("bwaCreateIdx","F")

#reference (1. default: all in one folder, 2. provide locations for bwa and picard, 3. create index files)
REF=config['ref']
if (ALIGNER == "BWAmem" or ALIGNER == "BWAaln") and os.path.isfile(REF + ".bwt"):
    print("found reference index file")
    BWAIDX = REF
else:
    if BWACREATEINDEX == "T":
        BWAIDX = ("resources/bwaIndex/"+ os.path.basename(REF))
    else:
        raise FileNotFoundError("We cannot find the bwa index files at "+ REF + ".bwt. \nThe ATLAS-pipeline can create that index for you if desired (bwaCreateIdx: T)")



BAMDPARAM=config.get("BAMDParam","")
MAPQUAL=config.get("mappingQ","30")
HEADCN=config.get("sequencingFacility","NA")

#load librarylist as dataframe
load = pd.read_csv(config['sampleFile'], sep='\t',  comment='#')

''' To discuss: input with "single" and "paired" instead of double rows for paired
dfS = load[load.SeqType == "single" ][['Sample','Lib','File']]
dfP1 = load[load.SeqType == "paired" ][['Sample','Lib','File']]
dfP2 = load[load.SeqType == "paired" ][['Sample','Lib','File']]

dfS['Read'] = "R1"
dfP1['Read'] = "R1"
dfP2['Read'] = "R2"

df = pd.concat([dfS, dfP1, dfP2]).sort_values(by=['File'])
df = df.set_index(['File','Read'])

pt = load[["Sample", "Lib", "File", "Path"]]
'''

# get paired- and single information and transform table
df = load[['Sample', 'Lib', 'File', 'Read']]
df = df.set_index(['File', 'Read'])
df.sort_values(['File', 'Read'], inplace=True)

pt = load.loc[load["Read"] == "R1", ["Sample", "Lib", "File", "Path"]]
pt = pt.set_index(['File'])

include: "fastQC.smk"
include: "trim.smk"
include: "align.smk"
include: "merge.smk"
include: "counts.smk"
include: "outfiles.smk"

rule file_summary:
    input:
        lambda wildcards: expand((OUTPATH +"/1.Gaia/02.fastqc/{{file}}_{read}_001_fastqc.html"), file = df.index.get_level_values(0), read = df.index.get_level_values(1)[df.index.get_level_values(0) == wildcards.file])
    output:
        file=(OUTPATH +"/1.Gaia/summaries/fil/{file}.summary.txt")
    shell:
        """
        echo -e '{input}' > {output.file}
        """
#########################################################################################################
#########################################################################################################
