#########################################################################################################
#########################################################################################################
# Author           : Ilektra Schulz
#                    ilektra.schulz@unifr.ch
# Source           : https://bitbucket.org/wegmannlab/atlas-pipeline/
#########################################################################################################
#########################################################################################################
rule FileStats:
    input:
        bam=(OUTPATH + "/1.Gaia/08.MkDup_per_lib/{file}.bam"),
        bai=(OUTPATH + "/1.Gaia/08.MkDup_per_lib/{file}.bam.bai")
    params:
        prefix=(OUTPATH + "/1.Gaia/x.FileStats/{file}"),
        BAMDParam=BAMDPARAM
    output:
        flag=(OUTPATH + "/1.Gaia/x.FileStats/{file}.flagstat"),
        bamd=(OUTPATH + "/1.Gaia/x.FileStats/{file}_diagnostics.txt"),
        alnLenHist=(OUTPATH + "/1.Gaia/x.FileStats/{file}_alignedLengthHistogram.txt"),
        fragHist=(OUTPATH + "/1.Gaia/x.FileStats/{file}_fragmentLengthHistogram.txt"),
        mqHist=(OUTPATH + "/1.Gaia/x.FileStats/{file}_mappingQualityHistogram.txt"),
        rdLenHist=(OUTPATH + "/1.Gaia/x.FileStats/{file}_readLengthHistogram.txt"),
        scLenHist=(OUTPATH + "/1.Gaia/x.FileStats/{file}_softClippedLengthHistogram.txt")
    message:
        "running BamDiagnostics...{wildcards.file}"
    conda:
        "../environments/environment_samtools.yaml"
    shell:
        """
        samtools flagstat {input.bam} > {output.flag}
        {config[atlas]} BAMDiagnostics --bam {input.bam} --out {params.prefix} --logFile {params.prefix}_BamDiagnostics.log {params.BAMDParam}
        """

rule SamStats:
    input:
        bam=(OUTPATH + "/1.Gaia/10.MkDup_per_sample/{sample}.bam"),
        bai=(OUTPATH + "/1.Gaia/10.MkDup_per_sample/{sample}.bam.bai")
    params:
        prefix=(OUTPATH + "/1.Gaia/x.SamStats/{sample}"),
        BAMDParam=BAMDPARAM
    output:
        flag=(OUTPATH + "/1.Gaia/x.SamStats/{sample}.flagstat"),
        bamd=(OUTPATH + "/1.Gaia/x.SamStats/{sample}_diagnostics.txt"),
        alnLenHist=(OUTPATH + "/1.Gaia/x.SamStats/{sample}_alignedLengthHistogram.txt"),
        fragHist=(OUTPATH + "/1.Gaia/x.SamStats/{sample}_fragmentLengthHistogram.txt"),
        mqHist=(OUTPATH + "/1.Gaia/x.SamStats/{sample}_mappingQualityHistogram.txt"),
        rdLenHist=(OUTPATH + "/1.Gaia/x.SamStats/{sample}_readLengthHistogram.txt"),
        scLenHist=(OUTPATH + "/1.Gaia/x.SamStats/{sample}_softClippedLengthHistogram.txt")
    message:
        "running BamDiagnostics...{wildcards.sample}"
    conda:
        "../environments/environment_samtools.yaml"
    shell:
        """
        samtools flagstat {input.bam} > {output.flag}
        {config[atlas]} BAMDiagnostics --bam {input.bam} --out {params.prefix} --logFile {params.prefix}_BamDiagnostics.log {params.BAMDParam}
        """

rule FileCounts:
    input:
        flagPrefilter=(OUTPATH + "/1.Gaia/06.newhead/{file}_newhead.flagstat"),
        fastqc=(OUTPATH + "/1.Gaia/02.fastqc/{file}_R1_001_fastqc.html"),
        flag=(OUTPATH + "/1.Gaia/x.FileStats/{file}.flagstat"),
        bamd=(OUTPATH + "/1.Gaia/x.FileStats/{file}_diagnostics.txt")
    params:
        trimmed1=(OUTPATH + "/1.Gaia/03.trimmed/{file}_R1_001.fastq.gz_trimming_report.txt"),
        trimmed2=(OUTPATH + "/1.Gaia/03.trimmed/{file}_R2_001.fastq.gz_trimming_report.txt"),
        fastqc2=(OUTPATH + "/1.Gaia/02.fastqc/{file}_R2_001_fastqc.html"),
        fastqc_post1=(OUTPATH + "/1.Gaia/02.fastqc/{file}_R1_001_trimmed_fastqc.html"),
        fastqc_post2=(OUTPATH + "/1.Gaia/02.fastqc/{file}_R2_001_trimmed_fastqc.html"),
        nr=lambda wildcards: len(df.index.get_level_values(1)[df.index.get_level_values(0) == wildcards.file]),
        adapter=ADAPTERS
    output:
        (OUTPATH + "/1.Gaia/y.FileCounts/{file}.counts")
    shell: 
       """ 
        BAMDhead=$( head -n1 {input.bamd} | cut -f 2-);
        echo "Name RawR1 RawR2 Raw Trimmed AfterTrimming Aligned Filtered Duplicates Percent_Endogenous Percent_Dup \
         Percent_Endog_NoDup $BAMDhead" > {output}; 
        name={wildcards.file}
        raw1=$(grep -o -P -m1 'Total Sequences.{{0,30}}' {input.fastqc} | awk -F '[><]' '{{print $5}}')
        if [[ {params.nr} == 1 ]]; then
            raw2=0
            if [[ {params.adapter} == "T" ]]; then 
            trimmed=$(grep "length cutoff" {params.trimmed1} | awk '{{print $14}}');  
            afterTrim=$(grep -o -P -m1 'Total Sequences.{{0,30}}' {params.fastqc_post1} | awk -F '[><]' '{{print $5}}'); 
            else trimmed=0; afterTrim=${{raw1}}; fi
        elif [[ {params.nr} == 2 ]]; then
            raw2=$(grep -o -P -m1 'Total Sequences.{{0,30}}' {params.fastqc2} | awk -F '[><]' '{{print $5}}')            
            if [[ {params.adapter} == "T" ]]; then 
            trimmed=$(grep "length cutoff" {params.trimmed2} | awk -F ":" '{{print $2}}' | awk -F "(" '{{print $1}}' | sed "s/ //g" | sed "s/\t//g"); 
            afterTrim1=$(grep -o -P -m1 'Total Sequences.{{0,30}}' {params.fastqc_post1} | awk -F '[><]' '{{print $5}}');
            afterTrim2=$(grep -o -P -m1 'Total Sequences.{{0,30}}' {params.fastqc_post2} | awk -F '[><]' '{{print $5}}');
            afterTrim=$((${{afterTrim1}}+${{afterTrim2}}))
            else trimmed=0; afterTrim1=0; afterTrim2=0; afterTrim=0 ;fi
        else echo "ERR something went wrong here. This is a bug. Please contact us."; exit 1; fi
        raw=$(($raw1 + $raw2))
        alignedPreFil=$(cat {input.flagPrefilter} | grep -m 1 "mapped" | awk '{{print $1}}')
        aligned=$(cat {input.flag} | grep -m 1 "mapped" | awk '{{print $1}}')
        pcEndog=$(gawk -v aln="${{aligned}}" -v rw="${{raw}}" 'BEGIN {{OFMT="%.2f";print ((aln*100)/rw)}}')
        duplicates=$(cat {input.flag} | grep duplicates | awk '{{print $1}}')
        if [ ${{aligned}} == 0 ]; then echo "WARNING: no sequences aligning to reference!"; pcDup=0; 
        else pcDup=$(gawk -v dup="${{duplicates}}" -v aln="${{aligned}}" 'BEGIN {{OFMT="%.2f";print ((dup*100)/aln)}}'); fi
        pcEndogNoDup=$(gawk -v Dup="${{duplicates}}" -v rw="${{raw}}" -v aln="${{aligned}}" 'BEGIN {{OFMT="%.2f";print (((aln-Dup)*100)/rw)}}')
        BAMD=$( awk '$1=="allReadGroups"' {input.bamd} | cut -f 2-);
        echo ${{name}} ${{raw1}} ${{raw2}} ${{raw}} ${{trimmed}} ${{afterTrim}} ${{alignedPreFil}} ${{aligned}} ${{duplicates}} \
         ${{pcEndog}} ${{pcDup}} ${{pcEndogNoDup}} ${{BAMD}} >> {output}
        """

rule SamCounts:
    input:
        cou=lambda wildcards: expand((OUTPATH + '/1.Gaia/y.FileCounts/{file}.counts'), file = pt.index[ pt.Sample == wildcards.sample]),
        flag=(OUTPATH + '/1.Gaia/x.SamStats/{sample}.flagstat'),
        bamd=(OUTPATH + '/1.Gaia/x.SamStats/{sample}_diagnostics.txt')
    output:
        (OUTPATH + "/1.Gaia/y.SamCounts/{sample}.counts")
    shell:
        """
        BAMDhead=$( head -n1 {input.bamd} | cut -f 2-);
        echo "Name Raw(Sum) Trimmed(Sum) AfterTrimming(Sum) Aligned(Sum) Aligned Duplicates(Sum) Duplicates \
        Percent_Endogenous Percent_Dup Percent_Endog_NoDup $BAMDhead" > {output}
        name={wildcards.sample}
        raw=$(cat {input.cou} | grep -v "Name Raw" | awk '{{s+=$4}} END {{print s}}')
        trim=$(cat {input.cou} | grep -v "Name Raw" | awk '{{s+=$5}} END {{print s}}')
        aftertrim=$(cat {input.cou} | grep -v "Name Raw" | awk '{{s+=$6}} END {{print s}}')
        alignedSum=$(cat {input.cou} | grep -v "Name Raw" | awk '{{s+=$7}} END {{print s}}')
        duplicatesSum=$(cat {input.cou} | awk '{{s+=$8}} END {{print s}}')
        aligned=$(cat {input.flag} | grep -m 1 "mapped" | awk '{{print $1}}')
        pcEndog=$(gawk -v aln="${{aligned}}" -v rw="${{raw}}" 'BEGIN {{OFMT="%.2f";print ((aln*100)/rw)}}')
        duplicates=$(cat {input.flag} | grep duplicates | awk '{{print $1}}')
        if [ ${{aligned}} == 0 ]; then echo "WARNING: ${{aligned}} sequences aligning to reference!"; pcDup=0; 
         else pcDup=$(gawk -v dup="${{duplicates}}" -v aln="${{aligned}}" 'BEGIN {{OFMT="%.2f";print ((dup*100)/aln)}}'); fi
        pcEndogNoDup=$(gawk -v Dup="${{duplicates}}" -v rw="${{raw}}" -v aln="${{aligned}}" 'BEGIN {{OFMT="%.2f";print (((aln-Dup)*100)/rw)}}') 
        BAMD=$( awk '$1=="allReadGroups"' {input.bamd} | cut -f 2-);
        echo ${{name}} ${{raw}} ${{trim}} ${{aftertrim}} ${{alignedSum}} ${{aligned}} ${{duplicatesSum}} ${{duplicates}}\
         ${{pcEndog}} ${{pcDup}} ${{pcEndogNoDup}} ${{BAMD}} >> {output}
        """
#########################################################################################################
#########################################################################################################
