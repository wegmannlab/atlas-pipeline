#########################################################################################################
#########################################################################################################
# Author           : Ilektra Schulz
#                    ilektra.schulz@unifr.ch
# Source           : https://bitbucket.org/wegmannlab/atlas-pipeline/
#########################################################################################################
#########################################################################################################
rule File_COUNT:
    input: 
        expand((OUTPATH +"/1.Gaia/y.FileCounts/{file}.counts"), zip, file=pt.index)
    output: 
        (OUTPATH +"/1.Gaia/outfiles/FILE_COUNTS")
    shell:
        """
        echo "Name Raw1 Raw2 Raw Trimmed AfterTrimming Aligned Duplicates Percent_Endogenous Percent_Dup Percent_Endog_NoDup \
        passedQC avgReadLength maxReadLength properPairs avgFragmentLength softClipped avgSoftClippedLength \
        avgUsableAlignedLength approximateDepth avgMappingQuality seqType" > {output};
        cat {input} | grep -v "AfterTrimming" >> {output}
        """

rule SamCOUNT:
    input: 
        expand((OUTPATH +"/1.Gaia/y.SamCounts/{sample}.counts"), zip, sample=pt.Sample)
    output: 
        (OUTPATH +"/1.Gaia/outfiles/SAM_COUNTS")
    shell:
        """
        echo "Name Raw(Sum) Trimmed(Sum) AfterTrimming(Sum) Aligned(Sum) Aligned Duplicates(Sum) Duplicates Percent_Endogenous \
        Percent_Dup Percent_Endog_NoDup passedQC avgReadLength maxReadLength properPairs avgFragmentLength softClipped \
        avgSoftClippedLength avgUsableAlignedLength approximateDepth avgMappingQuality seqType" > {output}
        cat {input} | grep -v "Name Raw(Sum) Trimmed(Sum)" >> {output}
        """

#make inputfile for Rhea, Perses or Pallas
def writeOutTable():
    outTable = load[['Sample']].drop_duplicates(ignore_index=True)
    outTable['Path'] = (OUTPATH +"/1.Gaia/10.MkDup_per_sample/")
    outTable['Target'] = "T"
    outTable['LocalReal'] = "T"
    outTable['Guidance'] = "T"
    outTable['GuidanceAlias'] = "F"
    outTable['GuidancePath'] = "F"
    outTable['poolRecal'] = "F"
    outTable['poolPMD'] = "F"
    outTable.to_csv((OUTPATH +'/1.Gaia/outfiles/Gaia_outTable.tsv'), sep="\t", index=False)

rule outTable:
    input: (OUTPATH +"/1.Gaia/outfiles/SAM_COUNTS")
    output: (OUTPATH +'/1.Gaia/outfiles/Gaia_outTable.tsv')
    run:
        writeOutTable()
#########################################################################################################
#########################################################################################################
