#########################################################################################################
#########################################################################################################
# Author           : Ilektra Schulz
#                    ilektra.schulz@unifr.ch
# Source           : https://bitbucket.org/wegmannlab/atlas-pipeline/
#########################################################################################################
#########################################################################################################
rule trim:
    input:
        lambda wildcards: expand("{path}{{file}}_{read}_001.fastq.gz", read=df.index.get_level_values(1)[df.index.get_level_values(0) == wildcards.file], path=pt.Path[pt.index == wildcards.file])
    params:
        ad1=ADAP1,
        ad2=ADAP2,
        qual=MINQUAL,
        leng=MINLEN,
        prefixT=(OUTPATH +"/1.Gaia/03.trimmed/"),
        prefixF=(OUTPATH +"/1.Gaia/02.fastqc/"),
    output:
        trimmed1=temp((OUTPATH +"/1.Gaia/03.trimmed/{file}_R1_001_trimmed.fq.gz")),
        trimmed2=temp((OUTPATH +"/1.Gaia/03.trimmed/{file}_R2_001_trimmed.fq.gz")),
        fastqc1=(OUTPATH +"/1.Gaia/02.fastqc/{file}_R1_001_trimmed_fastqc.html"),
        fastqc2=(OUTPATH +"/1.Gaia/02.fastqc/{file}_R2_001_trimmed_fastqc.html"),
        report=(OUTPATH +"/1.Gaia/03.trimmed/{file}_R1_001.fastq.gz_trimming_report.txt")
    threads: MAXTHREADS
    conda:
        "../environments/environment_trim.yaml"
    shell:
        """
        if [[ $(echo {input} | wc -w) == 1 ]]; then
            echo -e "found one input file for trimming: {input}"
            if [[ {params.ad1} == "standardAdapters" ]];then adapter=" " ; else adapter=$(echo "-a {params.ad1}"); fi
            trim_galore -j {threads} -o {params.prefixT} -q {params.qual} \
            --fastqc --fastqc_args '--outdir {params.prefixF}' --length {params.leng}  $adapter {input}
            echo "Attention! empty {output.trimmed2} and {output.fastqc2} files are being created for file consistency but will not be considered in downstream analysis."
            touch {output.trimmed2} {output.fastqc2}
        elif [[ $(echo {input} | wc -w) == 2 ]]; then
            echo -e "found two input files for trimming: {input}"
            if [[ {params.ad1} == "standardAdapters" ]]; then adapter1=" " ; else adapter1=$(echo "-a {params.ad1}"); fi
            if [[ {params.ad2} == "standardAdapters" ]]; then adapter2=" " ; else adapter2=$(echo "-a2 {params.ad2}"); fi
            trim_galore -j {threads}  --paired -o {params.prefixT} -q {params.qual} \
            --fastqc --fastqc_args '--outdir {params.prefixF} -d {resources.tmpdir}' --length {params.leng}  \
            --retain_unpaired $adapter1 $adapter2 {input} 
            mv {params.prefixT}{wildcards.file}_R1_001_val_1.fq.gz {output.trimmed1} 
            mv {params.prefixT}{wildcards.file}_R2_001_val_2.fq.gz {output.trimmed2} 
            mv {params.prefixF}{wildcards.file}_R1_001_val_1_fastqc.html {output.fastqc1}
            mv {params.prefixF}{wildcards.file}_R2_001_val_2_fastqc.html {output.fastqc2}
        fi
        """
#########################################################################################################
#########################################################################################################
