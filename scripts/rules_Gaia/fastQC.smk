#########################################################################################################
#########################################################################################################
# Author           : Ilektra Schulz
#                    ilektra.schulz@unifr.ch
# Source           : https://bitbucket.org/wegmannlab/atlas-pipeline/
#########################################################################################################
#########################################################################################################
rule fastqc:
    input:
        lambda wildcards: str(Path(pt.Path[wildcards.file])/f"{wildcards.file}_{wildcards.read}_001.fastq.gz")
    output:
        fastqc1=(OUTPATH +"/1.Gaia/02.fastqc/{file}_{read}_001_fastqc.html")
    params:
        prefix=(OUTPATH +"/1.Gaia/02.fastqc")
    conda:
        "../environments/environment_fastqc.yaml"
    shell:
        """
        fastqc {input} -o {params.prefix}
        """
#########################################################################################################
#########################################################################################################
