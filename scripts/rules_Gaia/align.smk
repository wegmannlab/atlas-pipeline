#########################################################################################################
#########################################################################################################
# Author           : Ilektra Schulz
#                    ilektra.schulz@unifr.ch
# Source           : https://bitbucket.org/wegmannlab/atlas-pipeline/
#########################################################################################################
#########################################################################################################
rule bwaIndex:
    input: REF
    output: BWAIDX+".bwt"
    params:
        prefix=BWAIDX
    conda: "../environments/environment_align.yaml"
    shell: "bwa index -p {params.prefix} {input}"


if ADAPTERS == "T":
    rule align:
        input:
            fastq=lambda wildcards: expand((OUTPATH +"/1.Gaia/03.trimmed/{{file}}_{read}_001_trimmed.fq.gz"),read=df.index.get_level_values(1)[df.index.get_level_values(0) == wildcards.file]),
            bwaidx=BWAIDX+".bwt",
            #TODO: need to execute fastqc early. this is a dirty fix. Think about a different work-around
            fastQC=lambda wildcards: expand((OUTPATH +"/1.Gaia/02.fastqc/{{file}}_{read}_001_fastqc.html"),read=df.index.get_level_values(1)[df.index.get_level_values(0) == wildcards.file])
        output:
            aln=temp((OUTPATH +"/1.Gaia/04.alignments/{file}_aligned.bam")),
            tst=temp((OUTPATH +"/1.Gaia/04.alignments/{file}_test.txt"))
        threads: MAXTHREADS
        params:
            prefix=(OUTPATH +"/1.Gaia/04.alignments/{file}"),
            aligner=ALIGNER,
            mapqual=MAPQUAL,
            bwaidx=BWAIDX
        conda:
            "../environments/environment_align.yaml"
        shell: 
            """
            if [ $(echo {input.fastq} | wc -w) == 2 ]; then in=$(echo {input.fastq}); else in=$(echo "{input.fastq} dummie") ;fi
            bash scripts/rules_Gaia/bwaMem.sh {params.aligner} {threads} {params.bwaidx} {params.mapqual} {resources.tmpdir} {params.prefix} $in 
            echo -e "done aligning" > {output.tst}
            """
elif ADAPTERS == "F":
    rule align:
        input:
            fastq=lambda wildcards: expand("{path}{{file}}_{read}_001.fastq.gz",
                read=df.index.get_level_values(1)[df.index.get_level_values(0) == wildcards.file],
                path=pt.Path[pt.index == wildcards.file]),
            bwaidx=BWAIDX+".bwt",
            #TODO: need to execute fastqc early. this is a dirty fix. Think about a different work-around
            fastQC=lambda wildcards: expand((OUTPATH +"/1.Gaia/02.fastqc/{{file}}_{read}_001_fastqc.html"),
                read=df.index.get_level_values(1)[df.index.get_level_values(0) == wildcards.file], path=pt.Path[pt.index == wildcards.file])
        output:
            aln=temp((OUTPATH +"/1.Gaia/04.alignments/{file}_aligned.bam")),
            tst=temp((OUTPATH +"/1.Gaia/04.alignments/{file}_test.txt"))
        threads: MAXTHREADS
        params:
            prefix=(OUTPATH +"/1.Gaia/04.alignments/{file}"),
            aligner=ALIGNER,
            mapqual=MAPQUAL,
            bwaidx=BWAIDX
        conda:
            "../environments/environment_align.yaml"
        shell: 
            """
            if [ $(echo {input.fastq} | wc -w) == 2 ]; then in=$(echo {input.fastq} ); else in=$(echo "{input.fastq} dummie");fi
            bash scripts/rules_Gaia/bwaMem.sh {params.aligner} {threads} {params.bwaidx} {params.mapqual}  {resources.tmpdir} {params.prefix} $in
            echo -e "done aligning" > {output.tst}
            """

rule sort_index:
    input:
        (OUTPATH +"/1.Gaia/04.alignments/{file}_aligned.bam")
    output:
        sort=temp((OUTPATH +"/1.Gaia/05.sort/{file}_sort.bam")),
        index=temp((OUTPATH +"/1.Gaia/05.sort/{file}_sort.bam.bai"))
    params:
        prefix=(OUTPATH +"/1.Gaia/05.sort/{file}")
    conda:
        "../environments/environment_samtools.yaml"
    shell:
        """
        rm -f {params.prefix}.bam.tmp.*.bam 
        samtools sort {input} -o {output.sort}
        samtools index {output.sort}
        """

rule newhead:
    input:
        bam=(OUTPATH +"/1.Gaia/05.sort/{file}_sort.bam"),
        bai=(OUTPATH +"/1.Gaia/05.sort/{file}_sort.bam.bai")
    params:
        ID="{file}",
        LB=lambda wildcards: pt[pt.index==wildcards.file]['Lib'].iloc[0],
        SM=lambda wildcards: pt.Sample[wildcards.file],
        CN=HEADCN
    output:
        bam=temp((OUTPATH +"/1.Gaia/06.newhead/{file}_newhead.bam"))
    conda:
        "../environments/environment_picard.yaml",
    shell:
        """
        set +e
        FASTQHEAD=$(samtools view {input.bam} | head -1 | sed 's/" "/-/g' | awk '{{print $1}}' | cut -d ":" -f 1,2,3,4);
        echo -e "LIB= {params.LB}, RGSM = {params.SM}, PU= $FASTQHEAD"
        picard AddOrReplaceReadGroups I={input.bam} O={output.bam} \
        ID={params.ID} LB={params.LB} PL=Illumina PU=${{FASTQHEAD}} SM={params.SM} CN={params.CN}
        exitcode=$?
        if [ $exitcode -eq 1 ]; then exit 1; else exit 0; fi
        """

rule flagstat:
    input:
        bam=(OUTPATH +"/1.Gaia/06.newhead/{file}_newhead.bam")
    params:
        file="{file}"
    output:
        out=(OUTPATH +"/1.Gaia/06.newhead/{file}_newhead.flagstat")
    conda:
        "../environments/environment_samtools.yaml"
    shell:
        "samtools flagstat {input.bam} > {output.out}"

rule filtering:
    input:
        bam=(OUTPATH +"/1.Gaia/06.newhead/{file}_newhead.bam")
    params:
        nr=lambda wildcards: len(df.index.get_level_values(1)[df.index.get_level_values(0) == wildcards.file])
    output:
        primary=temp((OUTPATH +"/1.Gaia/07.Filtered/{file}_primary.bam")),
        mapped=temp((OUTPATH +"/1.Gaia/07.Filtered/{file}_filtered.bam")),
        index=temp((OUTPATH +"/1.Gaia/07.Filtered/{file}_filtered.bam.bai"))
    conda:
        "../environments/environment_samtools.yaml"
    shell:
        """
        samtools view -bh -F 256 {input} > {output.primary}
        if [[ {params.nr} == 1 ]]; then
            samtools view -bh -F 4 {output.primary} > {output.mapped}
        elif [[ {params.nr} == 2 ]]; then
            samtools view -bh -f 3 {output.primary} > {output.mapped}
        else 
            echo "ERR something went wrong here. This is a bug. please contact us."
            exit 1
        fi
        samtools index {output.mapped}
        """

rule MkDup_File:
    input:
        (OUTPATH +"/1.Gaia/07.Filtered/{file}_filtered.bam")
    output:
        bam=(OUTPATH +"/1.Gaia/08.MkDup_per_lib/{file}.bam")
    params:
        java=JAVAMEM
    conda:
        "../environments/environment_picard.yaml"
    shell:
        """
        export _JAVA_OPTIONS='-Xmx{params.java}'
        set +e
        picard MarkDuplicates INPUT={input} OUTPUT={output.bam} METRICS_FILE=/dev/null REMOVE_DUPLICATES=false \
        AS=true VALIDATION_STRINGENCY=SILENT TMP_DIR=tmp/ TAGGING_POLICY=All
        exitcode=$?
        if [ $exitcode -eq 1 ]; then exit 1; else exit 0; fi
        """

rule MkDup_index1:
    input:
        bam=(OUTPATH +"/1.Gaia/08.MkDup_per_lib/{file}.bam")
    output:
        index=(OUTPATH +"/1.Gaia/08.MkDup_per_lib/{file}.bam.bai")
    conda:
        "../environments/environment_samtools.yaml"
    shell:
        """
        samtools index {input.bam}
        """
#########################################################################################################
#########################################################################################################

