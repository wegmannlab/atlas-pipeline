
#########################################################################################################
#########################################################################################################
# Author           : Ilektra Schulz
#                    ilektra.schulz@unifr.ch
# Source           : https://bitbucket.org/wegmannlab/atlas-pipeline/
#########################################################################################################
#########################################################################################################

rule merge:
    input:
        bam=lambda wildcards: expand((OUTPATH +'/1.Gaia/08.MkDup_per_lib/{file}.bam'), file = pt.index[ pt.Sample == wildcards.sample]),
        bai=lambda wildcards: expand((OUTPATH +'/1.Gaia/08.MkDup_per_lib/{file}.bam.bai'), file = pt.index[ pt.Sample == wildcards.sample])
    output:
        temp((OUTPATH +"/1.Gaia/09.merge/{sample}.bam"))
    conda:
        "../environments/environment_samtools.yaml"
    shell:
        """
        if [ $(echo {input.bam}|wc -w) == 1 ];then
        echo -e 'linking {input.bam} to {output}'
        ln -sr {input.bam} {output}
        else
        echo -e 'input is {input.bam} \n output is {output}'
        samtools merge -f {output} {input.bam}
        fi
        """

rule MkDup_sam:
    input:
        bam=(OUTPATH +"/1.Gaia/09.merge/{sample}.bam")
    output:
        bam=(OUTPATH +"/1.Gaia/10.MkDup_per_sample/{sample}.bam")
    params:
        nFiles=lambda wildcards: len(pt.index[ pt.Sample == wildcards.sample]),
        java=JAVAMEM
    conda:
        "../environments/environment_picard.yaml"
    shell:
        """
        if [[ {params.nFiles} == 1 ]]; then
        ln -sr {input.bam} {output.bam}
        else
        export _JAVA_OPTIONS='-Xmx{params.java}'
        set +e
        picard MarkDuplicates INPUT={input.bam} OUTPUT={output.bam} METRICS_FILE=/dev/null REMOVE_DUPLICATES=false \
        AS=true VALIDATION_STRINGENCY=SILENT TMP_DIR={resources.tmpdir} TAGGING_POLICY=All
        exitcode=$?
        if [ $exitcode -eq 1 ]; then exit 1; else exit 0; fi
        fi
        """

rule MkDup_index2:
    input:
        bam=(OUTPATH +"/1.Gaia/10.MkDup_per_sample/{sample}.bam")
    output:
        index=(OUTPATH +"/1.Gaia/10.MkDup_per_sample/{sample}.bam.bai")
    conda:
        "../environments/environment_samtools.yaml"
    shell:
        """
        samtools index {input.bam}
        """
#########################################################################################################
#########################################################################################################