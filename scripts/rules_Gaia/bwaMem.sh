#!/bin/bash -x
set -euo pipefail
IFS=$'\n\t'
#########################################################################################################
#########################################################################################################
# Author           : Ilektra Schulz
#                    ilektra.schulz@unifr.ch
# Source           : https://bitbucket.org/wegmannlab/atlas-pipeline/
#########################################################################################################
#########################################################################################################

aligner=$1
threads=$2
ref=$3
MQ=$4
tmp=$5
out=$6
input1=$7
input2=$8


if [[ "${input2}" == "dummie" ]]; then
   in=($input1)
else
 in=($input1); in+=($input2)
fi

now=$(echo `date +"%Y-%m-%d %T"` | tr ' ' '-' | tr ':' '-')
thisTmp=${tmp}/${out}_${now}

echo -e "Aligning reads with $aligner on $threads threads"
echo -e "ref=$ref \nMQ=$MQ\ntmp=$thisTmp\nout=$out\ninput=${in[@]}"


if [[ $aligner == "BWAmem" ]]; then
	bwa mem -M -t ${threads}  $ref  ${in[@]} | samtools view -bSh -T $thisTmp -q $MQ - > ${out}_aligned.bam
elif [[ $aligner == "BWAaln" ]]; then
	if [ ${#in[@]} == 2 ]; then
		bwa aln -t ${threads} -l 1024 -n 0.01 -o 2 $ref  $input1 > ${out}_R1.sai
		bwa aln -t ${threads} -l 1024 -n 0.01 -o 2 $ref $input2 > ${out}_R2.sai
		bwa sampe $ref ${out}_R1.sai ${out}_R2.sai  $input1  $input2 | samtools view -bSh -T $thisTmp -q $MQ - > ${out}_aligned.bam
		rm  ${out}_R1.sai ${out}_R2.sai
	else
		bwa aln -t ${threads} -l 1024 -n 0.01 -o 2 $ref  $input1 > ${out}.sai
		bwa samse $ref ${out}.sai  ${in[0]} | samtools view -bSh -T $thisTmp  -q $MQ - > ${out}_aligned.bam
		rm ${out}.sai
	fi
fi



#########################################################################################################
#########################################################################################################
