#########################################################################################################
#########################################################################################################
# Author           : Ilektra Schulz
#                    ilektra.schulz@unifr.ch
# Source           : https://bitbucket.org/wegmannlab/atlas-pipeline/
#########################################################################################################
#########################################################################################################
rule bamdiagnostics1:
    input:
        bam=lambda wildcards: str(Path(df["Path"][wildcards.sample])/f"{wildcards.sample}.bam")
    params:
        prefix=(OUTPATH +"/3.Perses/BAMDiagnostics_inputFiles/{sample}"),
        atParam=ATLASPARAMS,
        bdParam=BAMDPARAMS
    output:
        (OUTPATH +"/3.Perses/BAMDiagnostics_inputFiles/{sample}_diagnostics.txt"),
        (OUTPATH +"/3.Perses/BAMDiagnostics_inputFiles/{sample}_readLengthHistogram.txt"),
        (OUTPATH +"/3.Perses/BAMDiagnostics_inputFiles/{sample}_alignedLengthHistogram.txt"),
        (OUTPATH +"/3.Perses/BAMDiagnostics_inputFiles/{sample}_softClippedLengthHistogram.txt"),
        (OUTPATH +"/3.Perses/BAMDiagnostics_inputFiles/{sample}_fragmentLengthHistogram.txt"),
        (OUTPATH +"/3.Perses/BAMDiagnostics_inputFiles/{sample}_mappingQualityHistogram.txt"),
    message:
        "running BamDiagnostics...{wildcards.sample}"
    shell:
        """
	#module load vital-it/7 # old cluster
	#module add Development/gcc/9.2.1; # old cluster
	{config[atlas]} BAMDiagnostics --bam {input.bam} --out {params.prefix} {params.atParam} {params.bdParam} 
	"""

rule bamdiagnostics2:
    input:
        bam=(OUTPATH +"/3.Perses/mergeOverlappingReads/{sample}_merged.bam")
    params:
        prefix=(OUTPATH +"/3.Perses/BAMDiagnostics_afterReadsMerge/{sample}"),
        atParam=ATLASPARAMS,
        bdParam=BAMDPARAMS
    output:
        (OUTPATH +"/3.Perses/BAMDiagnostics_afterReadsMerge/{sample}_diagnostics.txt"),
        (OUTPATH +"/3.Perses/BAMDiagnostics_afterReadsMerge/{sample}_readLengthHistogram.txt"),
        (OUTPATH +"/3.Perses/BAMDiagnostics_afterReadsMerge/{sample}_alignedLengthHistogram.txt"),
        (OUTPATH +"/3.Perses/BAMDiagnostics_afterReadsMerge/{sample}_softClippedLengthHistogram.txt"),
        (OUTPATH +"/3.Perses/BAMDiagnostics_afterReadsMerge/{sample}_fragmentLengthHistogram.txt"),
        (OUTPATH +"/3.Perses/BAMDiagnostics_afterReadsMerge/{sample}_mappingQualityHistogram.txt"),
    message:
        "running BamDiagnostics...{wildcards.sample}"
    shell:
        """ 
	
	{config[atlas]} BAMDiagnostics --bam {input.bam} --out {params.prefix}  {params.atParam} {params.bdParam} 
	"""
#########################################################################################################
#########################################################################################################
