#########################################################################################################
#########################################################################################################
# Author           : Ilektra Schulz
#                    ilektra.schulz@unifr.ch
# Source           : https://bitbucket.org/wegmannlab/atlas-pipeline/
#########################################################################################################
#########################################################################################################

rule PMD_inputFile:
    input:
        readsMergeRG=(OUTPATH +"/3.Perses/BAMDiagnostics_inputFiles/{sample}_diagnostics.txt")
    params:
        poolRG=lambda wildcards: df.poolPMD[df.index==wildcards.sample][wildcards.sample]
    output:
        PMD_input=(OUTPATH +"/3.Perses/readgroups/{sample}_PMD_inputfile.txt")
    run:
        diag = pd.read_csv(input.readsMergeRG,sep="\t",comment='#')
        rgs=params.poolRG
        if rgs == "F" or rgs =="FALSE":
            print("no merging of readgroups")
            Path(output.PMD_input).touch()
        else:
            RGlist = [i.split(",") for i in rgs.split(";")]
            with open(output.PMD_input,'w') as f:
                f.writelines("readGroup\tpoolWith\n")
                for line in RGlist:
                    print(line)
                    if len(line) == 1:
                        print("WARNING: No readgroup specified to merge with " + line + "! Will not consider readgroup for merging")
                    else:
                        for i in range(1,len(line)):
                            f.writelines(line[i] + "\t" + line[0] + "\n")
                        trunk = diag.readGroup[diag.readGroup.isin(line) & (diag.seqType == 'single-end')].tolist()
                        if len(trunk) > 1:
                            for i in range(1,len(trunk)):
                                f.writelines(trunk[i] + "_truncated\t" + trunk[0] + "_truncated\n")


rule recal_inputFile:
    input:
        readsMergeRG=(OUTPATH +"/3.Perses/BAMDiagnostics_inputFiles/{sample}_diagnostics.txt")
    params:
        poolRG=lambda wildcards: df.poolRecal[df.index==wildcards.sample][wildcards.sample],
    output:
        recal_input=(OUTPATH +"/3.Perses/readgroups/{sample}_recal_inputfile.txt")
    run:
        diag = pd.read_csv(input.readsMergeRG,sep="\t",comment='#')
        rgs = params.poolRG
        if rgs == "F" or rgs == "FALSE":
            print("no merging of readgroups")
            Path(output.recal_input).touch()
        else:
            RGlist = [i.split(",") for i in rgs.split(";")]
            with open(output.recal_input,'w') as f:
                f.writelines("readGroup\tpoolWith\n")
                for line in RGlist:
                    if len(line) == 1:
                        print("WARNING: No readgroup specified to merge with " + line + "! Will not consider readgroup for merging")
                    else:
                        for i in range(1,len(line)):
                            f.writelines(line[i] + "\t" + line[0] + "\n")
                        trunk = diag.readGroup[diag.readGroup.isin(line) & (diag.seqType == 'single')].tolist()
                        if len(trunk) > 1:
                            for i in range(0,len(trunk)):
                                f.writelines(trunk[i] + "_truncated\t" + trunk[i]+"\n")


rule estimateErrors:
    input:
        bam=(OUTPATH +"/3.Perses/mergeOverlappingReads/{sample}_merged.bam"),
        poolPMD=(OUTPATH +"/3.Perses/readgroups/{sample}_PMD_inputfile.txt"),
        poolRecal=(OUTPATH +"/3.Perses/readgroups/{sample}_recal_inputfile.txt")
    params:
        prefix=(OUTPATH +"/3.Perses/estimateErrors/{sample}_merged"),
        atParam=ATLASPARAMS,
        recFormat=RECALFORMAT,
        recForm=RECALFORM,
        pmdForm=PMDFORM,
	estimerrParam=ESTIMERRPARAMS
    output:
        estErr=(OUTPATH +"/3.Perses/estimateErrors/{sample}_merged_RGInfo.json")
    shell:
        """
    recForm=params.recForm
    if [ $recForm == "all" ]; then
        echo "all readgroups will be merged for sequencing error recalibration"
	recal="--poolRecal all";
    elif [ -s {input.poolRecal} ];
        then recal=$(echo "--poolRecal {input.poolRecal}"); 
    else 
        echo "no merging of readgroups for sequencing error recalibration"
        recal=" "; fi

    pmdForm=params.pmdForm
    if [ pmdForm == "all" ]; then
        echo "all readgroups will be merged for pmd estimation"
        pmd="--poolPMD all";
    elif [ -s {input.poolPMD} ];
        then pmd=$(echo "--poolPMD {input.poolPMD}"); 
    else 
        echo "no merging of readgroups for pmd estimation"
        pmd=" "; fi

    {config[atlas]} estimateErrors \
        --minDeltaLL 10 \
        --bam {input.bam} \
        --fasta {config[ref]} \
        --out {params.prefix} --logFile {params.prefix}.log\
        --{params.recFormat} {config[recalSites]} {params.atParam} {params.estimerrParam} ${{pmd}} ${{recal}}
        """

rule recalBAM:
    input:
        bam=(OUTPATH +"/3.Perses/mergeOverlappingReads/{sample}_merged.bam"),
        rginfo=(OUTPATH +"/3.Perses/estimateErrors/{sample}_merged_RGInfo.json")
    params:
        prefix=(OUTPATH +"/3.Perses/recalBAM/{sample}_merged"),
        atParam=ATLASPARAMS,
        recBParam=RECBAMPARAMS
    output:
        (OUTPATH +"/3.Perses/recalBAM/{sample}_merged_recalibrated.bam")
    message:
        "Creating recalibrated bamfile on {wildcards.sample}"
    shell:
        """
        {config[atlas]} filterBAM --bam {input.bam} --RGInfo {input.rginfo}  \
        --out {params.prefix} {params.atParam} {params.recBParam}
        mv {params.prefix}_filtered.bam {params.prefix}_recalibrated.bam
        mv {params.prefix}_filtered.bam.bai {params.prefix}_recalibrated.bam.bai
        """
#########################################################################################################
#########################################################################################################
