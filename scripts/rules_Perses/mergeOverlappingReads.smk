#########################################################################################################
#########################################################################################################
# Author           : Ilektra Schulz
#                    ilektra.schulz@unifr.ch
# Source           : https://bitbucket.org/wegmannlab/atlas-pipeline/
#########################################################################################################
#########################################################################################################

def mergeORReadGroups(wildcards):
    defaultRG=(OUTPATH +"/3.Perses/BAMDiagnostics_inputFiles/{sample}_diagnostics.txt")
    if "rgFile" in df[df.index==wildcards.sample]:
        file=df.rgFile[df.index==wildcards.sample].to_string(index=False, header=False)
        if os.path.exists(file):
            return[file]
        elif file == "F":
            return[defaultRG]
        else:
            raise ValueError("Readgroup file for sample " + wildcards.sample + " not found: " + file)
    else:
        return[defaultRG]

rule mergeOverlappingReads:
    input:
        bam=lambda wildcards: str(Path(df["Path"][wildcards.sample])/f"{wildcards.sample}.bam"),
        rgs=mergeORReadGroups
    params:
        prefix=(OUTPATH +"/3.Perses/mergeOverlappingReads/{sample}"),
        atParam=ATLASPARAMS,
        smParam=READMERGEPARAMS
    output:
        bam=(OUTPATH +"/3.Perses/mergeOverlappingReads/{sample}_merged.bam"),
        bai=(OUTPATH +"/3.Perses/mergeOverlappingReads/{sample}_merged.bam.bai")
    message:
        "merging reads of paired end readgroups of {wildcards.sample} ....."
    shell:
        """
        {config[atlas]} mergeOverlappingReads --bam {input.bam} --out {params.prefix} --readGroupSettings {input.rgs} \
        {params.atParam} {params.smParam}
        """
#########################################################################################################
#########################################################################################################
