
#########################################################################################################
#----------------------------------------------PERSES---------------------------------------------------#
#########################################################################################################
# Author           : Ilektra Schulz
#                    ilektra.schulz@unifr.ch
# Source           : https://bitbucket.org/wegmannlab/atlas-pipeline/
#########################################################################################################
#########################################################################################################

#load samplelist as dataframe
if config['sampleFile'] == "fromGaia":
    SLIST = (OUTPATH +"/1.Gaia/outfiles/Gaia_outTable.tsv")
elif config['sampleFile'] == "fromRhea":
    SLIST = (OUTPATH +"/2.Rhea/outfiles/Rhea_outTable.tsv")
else:
    SLIST = config['sampleFile']

df = pd.read_csv(SLIST, sep='\t', index_col='Sample', comment='#')

#defaults
ATLASPARAMS = config.get("atlasParams","")
BAMDPARAMS = config.get("BAMDParams","")
READMERGEPARAMS  = config.get("mergeOverlappingReadsParams","")
ERREST = config.get("estimateErrors","T")
PMDFORM = config.get("pmdForm", "")
RECALFORM  = config.get("recalForm","")
RECALFORMAT  = config.get("recalFormat","regions")
ESTIMERRPARAMS = config.get("estimErrParams","")
RECBAM = config.get("recalBAM","F")
RECBAMPARAMS = config.get("recalBamParams","")

include: "BAMD.smk"
include: "mergeOverlappingReads.smk"
include: "estimateErrors.smk"

inputFiles = [(OUTPATH +"/3.Perses/BAMDiagnostics_inputFiles/{sample}_diagnostics.txt"), (OUTPATH +"/3.Perses/BAMDiagnostics_afterReadsMerge/{sample}_diagnostics.txt")]

if ERREST == "T":
    inputFiles.append((OUTPATH +"/3.Perses/estimateErrors/{sample}_merged_RGInfo.json"))
if ERREST == "F":
    RECBAM == "F"

if RECBAM == "T":
    inputFiles.append((OUTPATH +"/3.Perses/recalBAM/{sample}_merged_recalibrated.bam"))

rule summary:
    input:
        inputFiles
    output:
        temp((OUTPATH +"/3.Perses/summaries/{sample}.summary.txt"))
    shell:
        """
        echo {input} > {output}
        """

def writeOutTable():
    outTable = df.reset_index(level=0)[['Sample']] + "_merged"
    outTable['Path'] = (OUTPATH + "/3.Perses/mergeOverlappingReads/")
    outTable['Pop'] = "Pop1"
    outTable.to_csv((OUTPATH + '/3.Perses/outfiles/Perses_outTable.tsv'),sep="\t",index=False)

rule outTable:
    input: expand((OUTPATH + "/3.Perses/summaries/{sample}.summary.txt"), zip, sample=df.index)
    output: (OUTPATH + '/3.Perses/outfiles/Perses_outTable.tsv')
    run:
        writeOutTable()
#########################################################################################################
#########################################################################################################
