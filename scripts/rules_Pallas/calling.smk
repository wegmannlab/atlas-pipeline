#########################################################################################################
#########################################################################################################
# Author           : Ilektra Schulz
#                    ilektra.schulz@unifr.ch
# Source           : https://bitbucket.org/wegmannlab/atlas-pipeline/
#########################################################################################################
#########################################################################################################
outPath="otherresult/"

rule callMLE:
    input:
        bam=inputBAM(),
        estimateErrors=estimateErrorsInput
    params:
        prefix=(OUTPATH + "/4.Pallas/call/callMLE/{sample}"),
        atlparam=ATLASPARAMS,
        callparam=CALLPARAMS
    output:
        (OUTPATH + "/4.Pallas/call/callMLE/{sample}_calls_maximumLikelihood.vcf.gz")
    message:
        "MLE-caller...{wildcards.sample}..."
    shell:
        """
        if [ -z {input.estimateErrors} ]; then estimateErrors=""; else estimateErrors="--RGInfo {input.estimateErrors}"; fi
        {config[atlas]} call --method MLE --fasta {config[ref]} \
        --bam {input.bam}  $estimateErrors --out {params.prefix} {params.atlparam}  {params.callparam} 
        """
 
rule AllelePresence:
    input:
        bam=inputBAM(),
        estimateErrors=estimateErrorsInput
    params:
        prefix=(OUTPATH + "/4.Pallas/call/AllelePresence/{sample}"),
        atlparam=ATLASPARAMS,
        callparam=CALLPARAMS
    output:
        (OUTPATH + "/4.Pallas/call/AllelePresence/{sample}_calls_allelePresence.vcf.gz"),
        (OUTPATH + "/4.Pallas/call/AllelePresence/{sample}_theta_estimates.txt.gz)")
    message:
        "calling allele presence...{wildcards.sample}"
    shell:
        """
        if [ -z {input.estimateErrors} ]; then estimateErrors=""; else estimateErrors="--RGInfo {input.estimateErrors}"; fi        
        {config[atlas]} call --method allelePresence --bam {input.bam} \
        --fasta {config[ref]}  $estimateErrors --out {params.prefix}  {params.atlparam}  {params.callparam}
        """

rule callBayes:
    input:
        bam=inputBAM(),
        estimateErrors=estimateErrorsInput
    params:
        prefix=(OUTPATH + "/4.Pallas/call/callBayes/{sample}"),
        atlparam=ATLASPARAMS,
        callparam=CALLPARAMS
    output:
        (OUTPATH + "/4.Pallas/call/callBayes/{sample}_calls_maximumAPosteriori.vcf.gz"),
        (OUTPATH + "/4.Pallas/call/callBayes/{sample}_theta_estimates.txt.gz"),
    message:
        "Calling with beyesian method.....{wildcards.sample}"
    shell:
        """
        if [ -z {input.estimateErrors} ]; then estimateErrors=""; else estimateErrors="--RGInfo {input.estimateErrors}"; fi
        {config[atlas]} call --method Bayesian --bam {input.bam} --fasta {config[ref]}  $estimateErrors  \
        --out {params.prefix} {params.atlparam} {params.callparam}
        """
#########################################################################################################
#########################################################################################################
