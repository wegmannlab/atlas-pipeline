#########################################################################################################
#########################################################################################################
# Author           : Ilektra Schulz
#                    ilektra.schulz@unifr.ch
# Source           : https://bitbucket.org/wegmannlab/atlas-pipeline/
#########################################################################################################
#########################################################################################################
if PARA == "F":
    rule GLF:
        input:
            bam=inputBAM(),
            estimateErrors=estimateErrorsInput
        params:
            prefix=(OUTPATH +"/4.Pallas/GLF/{sample}"),
            atlparam=ATLASPARAMS,
            glfparam=GLFPARAMS
        output:
            (OUTPATH +"/4.Pallas/GLF/{sample}.glf.gz")
        conda:
            "../environments/environment_gcc.yaml"
        message:
            "GLF for ..... {wildcards.sample}..."
        resources:
            tmpdir=TMPDIR
        shell:
            """
            if [ -z {input.estimateErrors} ]; then estimateErrors=""; else estimateErrors="--RGInfo {input.estimateErrors}"; fi
            {config[atlas]} GLF --bam {input.bam} --out {params.prefix} $estimateErrors {params.atlparam} {params.glfparam}
            """
else:
    rule GLF:
        input:
            bam=inputBAM(),
            estimateErrors=estimateErrorsInput
        params:
            prefix=(OUTPATH +"/4.Pallas/GLF/PerChrom/{sample}_bin-{binNum}_"),
            chrom=lambda wildcards: ",".join([str(i) for i in binDict[int(wildcards.binNum)]]),
            atlparam=ATLASPARAMS,
            glfparam=GLFPARAMS
        output:
            glf=(OUTPATH +"/4.Pallas/GLF/PerChrom/{sample}_bin-{binNum}_.glf.gz"),
            argfile=(OUTPATH +"/4.Pallas/GLF/PerChrom/{sample}_bin-{binNum}_argfile.txt")
        conda:
            "../environments/environment_gcc.yaml"
        message:
            "GLF for ..... {wildcards.sample}..."
        resources:
            tmpdir=TMPDIR
        shell:
            """
            echo -e "task GLF \\nchr {params.chrom}" > {output.argfile}
            if [ -z {input.estimateErrors} ]; then estimateErrors=""; else estimateErrors="--RGInfo {input.estimateErrors}"; fi
            {config[atlas]} {output.argfile} --bam {input.bam} --out {params.prefix} $estimateErrors {params.atlparam} {params.glfparam}
            """
        
    rule allGLF:
        input: 
            glfs=expand((OUTPATH +"/4.Pallas/GLF/PerChrom/{{sample}}_bin-{binNum}_.glf.gz"), zip, binNum=binList)
        params:
            prefix=(OUTPATH +"/4.Pallas/GLF/")
        output: 
            glfs=temp(OUTPATH +"/4.Pallas/GLF/{sample}.GLFPaths.txt")
        shell:
            "ls {input} > {output}"

#########################################################################################################
#########################################################################################################
