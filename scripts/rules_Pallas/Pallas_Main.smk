
#########################################################################################################
#----------------------------------------------PALLAS---------------------------------------------------#
#########################################################################################################
# Author           : Ilektra Schulz
#                    ilektra.schulz@unifr.ch
# Source           : https://bitbucket.org/wegmannlab/atlas-pipeline/
#########################################################################################################
#########################################################################################################

#load samplelist as dataframe (takes same samplelist as Perses by default)
if config['sampleFile'] == "fromGaia":
    SLIST = (OUTPATH + "/1.Gaia/outfiles/Gaia_outTable.tsv")
elif config['sampleFile'] == "fromRhea":
    SLIST = (OUTPATH + "/2.Rhea/outfiles/Rhea_outTable.tsv")
elif config['sampleFile'] == "fromPerses":
    SLIST = (OUTPATH + "/3.Perses/outfiles/Perses_outTable.tsv")
else:
    SLIST = config['sampleFile']

df = pd.read_csv(SLIST, sep='\t', index_col='Sample', comment='#')
samplelist=df.index.tolist()
NSAM=len(samplelist)

#defaults
ATLASPARAMS = config.get("atlasParams","")

ERREST=config.get("estimateErrors", "F")

SEX = config.get("sex","F")

CALL = config.get("call","F")
CALLPARAMS = config.get("callParams","")

GLF = config.get("glf","F")
GLFPARAMS = config.get("glfParams","")

THETA = config.get("theta","F")
THETAPARAMS = config.get("thetaParams","")

PARA = config.get("parallelize","F")
BINCOUNT = config.get("binCount","F")
CHRPAR = config.get("chromPar","all")

MAMI = config.get("maMi","F")
MAMIPARAMS = config.get("maMiParams","")
MAMIOUT = config.get("maMiOut","MajorMinor")

BEAGLE = config.get("beagle","F")
BEAGLEPARAMS = config.get("beagleParams","")

PCA = config.get("PCA","F")
PCANGSD = config.get("PCAngsd","pcangsd")
PCAPARAMS = config.get("PCAParams","")

THREADS = config.get("threads",8)

# to estimate GLF and Major Minor per bin, distribute bins equally
include: "../rules_general/contigs.smk"
#include: "init.smk"

# if estimateErrors should be taken into account, check for external files or Perses input.
def estimateErrorsInput(wildcards):
    if ERREST == "T":
        defaultEstimateErrors = (OUTPATH + "/3.Perses/estimateErrors/{sample}_RGInfo.json")
        if "estimateErrorsFile" in df[df.index==wildcards.sample]:
            file=df.estimateErrorsFile[df.index==wildcards.sample].to_string(index=False, header=False)
            if os.path.exists(file):
                return[file]
            elif file == "F":
                return[defaultEstimateErrors]
            else:
                raise ValueError("estimateErrorsFile for sample " + wildcards.sample + " not found: " + file)
        else:
            return[defaultEstimateErrors]
    else:
        return[]

def inputBAM():
    return lambda wildcards: str(Path(df["Path"][wildcards.sample])/f"{wildcards.sample}.bam")


# snakefiles
include: "calling.smk"
include: "glf.smk"
include: "estimateTheta.smk"
include: "additionalAnalysis.smk"
include: "inbreeding.smk"
include: "majorMinor.smk"
include: "PCA.smk"

# define paths by defining final inputfiles depending on config-parameters.
inputFiles = [] #for rule summary (sample-wies)
finalInputFiles = [] #for rule all (multi-sample)

if SEX == "T":
    inputFiles.append((OUTPATH + "/4.Pallas/sex/{sample}.Sex"))

if CALL == "Bayes":
    inputFiles.append((OUTPATH + "/4.Pallas/call/callBayes/{sample}_calls_maximumAPosteriori.vcf.gz"))
    inputFiles.append((OUTPATH +"/4.Pallas/call/callBayes/{sample}_theta_estimates.txt.gz"))
elif CALL == "MLE":
    inputFiles.append(OUTPATH + "/4.Pallas/call/callMLE/{sample}_calls_maximumLikelihood.vcf.gz")
elif CALL == "allelePresence":
    inputFiles.append((OUTPATH +"/4.Pallas/call/AllelePresence/{sample}_calls_allelePresence.vcf.gz"))
    inputFiles.append((OUTPATH +"/4.Pallas/call/AllelePresence/{sample}_theta_estimates.txt.gz"))

if PCA == "T":
    BEAGLE = "T"
    finalInputFiles.append(OUTPATH +"/4.Pallas/PCA/"+MAMIOUT+".pdf")

if BEAGLE == "T":
    MAMI = "T"
    finalInputFiles.append(OUTPATH +"/4.Pallas/PCA/"+MAMIOUT+".beagle.gz")

if MAMI == "T":
    GLF = "T"
    finalInputFiles.append((OUTPATH +"/4.Pallas/MajorMinor/"+MAMIOUT+".vcf.gz"))
    #check that samplesize is smaller than ulimit-3


if GLF == "T":
    if PARA == "F":
        inputFiles.append(OUTPATH +"/4.Pallas/GLF/{sample}.glf.gz")
    else:
        inputFiles.append(OUTPATH +"/4.Pallas/GLF/{sample}.GLFPaths.txt")

if THETA == "genomeWide":
    inputFiles.append((OUTPATH +"/4.Pallas/theta/genomeWide/{sample}_thetaGenomeWide.txt.gz"))
elif THETA == "window":
    inputFiles.append((OUTPATH +"/4.Pallas/theta/wind/{sample}_thetaPerWindow.txt.gz"))

rule summary:
    input:
        inputFiles
    output:
        temp((OUTPATH +"/4.Pallas/summaries/{sample}.summary.txt"))
    shell:
        """
        echo {input} > {output}
        """
#########################################################################################################
#########################################################################################################
