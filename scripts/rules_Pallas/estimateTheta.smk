#########################################################################################################
#########################################################################################################
# Author           : Ilektra Schulz
#                    ilektra.schulz@unifr.ch
# Source           : https://bitbucket.org/wegmannlab/atlas-pipeline/
#########################################################################################################
#########################################################################################################
rule EstimateTheta:
    input:
        bam=inputBAM(),
        estimateErrors=estimateErrorsInput
    params:
        prefix=(OUTPATH +"/4.Pallas/theta/wind/{sample}"),
        atlparam=ATLASPARAMS,
        thetaparam=THETAPARAMS
    output:
        (OUTPATH +"/4.Pallas/theta/wind/{sample}_thetaPerWindow.txt.gz")
    message:
        "Estimating theta for {wildcards.sample}...per window"
    shell:
        """
        if [ -z {input.estimateErrors} ]; then estimateErrors=""; else estimateErrors="--RGInfo {input.estimateErrors}"; fi
        {config[atlas]} theta --bam {input.bam}  --out {params.prefix}  $estimateErrors  {params.atlparam}  {params.thetaparam} 
        """

rule EstimateGlobalTheta:
    input:
        bam=inputBAM(),
        estimateErrors=estimateErrorsInput
    params:
        prefix=(OUTPATH +"/4.Pallas/theta/genomeWide/{sample}"),
        atlparam=ATLASPARAMS,
        thetaparam=THETAPARAMS
    output:
        (OUTPATH +"/4.Pallas/theta/genomeWide/{sample}_thetaGenomeWide.txt.gz")
    message:
        "Estimating genomeWide theta for {wildcards.sample}..."
    shell:
        """
        if [ -z {input.estimateErrors} ]; then estimateErrors=""; else estimateErrors="--RGInfo {input.estimateErrors}"; fi
        {config[atlas]} thetaGenomeWide --bam {input.bam} --out {params.prefix}  $estimateErrors  {params.atlparam}  {params.thetaparam}
        """

rule getGlobalTheta:
    input:
        (OUTPATH +"/4.Pallas/theta/genomeWide/{sample}_theta_estimates.txt")
    output:
        dynamic((OUTPATH +"/4.Pallas/theta/genomeWide/{sample}_Global_Theta.txt"))
    shell:
        "cat {input} | grep genome-wide | cut -f11 > {output}"

#########################################################################################################
#########################################################################################################
