#########################################################################################################
#########################################################################################################
# Author           : Ilektra Schulz
#                    ilektra.schulz@unifr.ch
# Source           : https://bitbucket.org/wegmannlab/atlas-pipeline/
#########################################################################################################
#########################################################################################################
rule sex:
    input:
        bam=inputBAM()
    params:
        prefix=(OUTPATH +"/4.Pallas/sex/{sample}")
    output:
        (OUTPATH +"/4.Pallas/sex/{sample}.Sex")
    message:
        "Estimating the genetic sex with Skoglund(2013) on {wildcards.sample}. \n WARNING: only for human data!"
    shell:
        "samtools view -q 30 {input.bam} | python scripts/yjasc_3752_ry_compute.py >> {params.prefix}.Sex "
#########################################################################################################
#########################################################################################################
