
#########################################################################################################
#-----------------------------------------------GAIA----------------------------------------------------#
#########################################################################################################
# Author           : Ilektra Schulz
#                    ilektra.schulz@unifr.ch
# Source           : https://bitbucket.org/wegmannlab/atlas-pipeline/
#########################################################################################################
#########################################################################################################
if PARA == "F":
    rule majorMinor:
        input:
            glf=expand((OUTPATH +"/4.Pallas/GLF/{sample}.glf.gz"), sample=df.index)
        params:
            prefix=(OUTPATH + "/4.Pallas/MajorMinor/" + MAMIOUT),
            atlParam=ATLASPARAMS,
            maMiParam=MAMIPARAMS,
            nSam=(len(df.index)-3),
            ulim=(len(df.index)+5)
        output:
            (OUTPATH +"/4.Pallas/MajorMinor/"+ MAMIOUT + ".vcf.gz")
        conda:
            "../environments/environment_gcc.yaml"
        resources:
            tmpdir=TMPDIR
        shell:
            """
            if (( $(ulimit -Sn) < {params.nSam} )); then echo "INFO: increasing system ulimit to sample size!"; ulimit -n {params.ulim} ;fi
            {config[atlas]} majorMinor --glf $(echo {input} | tr [:blank:] ",") --phredLik  --out {params.prefix}  {params.atlParam}  {params.maMiParam}
            """
else:
    rule majorMinor:
        input:
            glfs=expand((OUTPATH +"/4.Pallas/GLF/PerChrom/{sample}_bin-{{binNum}}_.glf.gz"), zip, sample=df.index)
        params:
            prefix=(OUTPATH +"/4.Pallas/MajorMinor/PerChrom/"+MAMIOUT+"_bin-{binNum}"),
            names=lambda wildcards: ",".join([str(i) for i in df.index.to_list()]) ,
            atlParam=ATLASPARAMS,
            maMiParam=MAMIPARAMS,
            nSam=(len(df.index)-3),
            ulim=(len(df.index)+5)
        conda:
            "../environments/environment_gcc.yaml"
        resources:
            tmpdir=TMPDIR
        output:
            (OUTPATH +"/4.Pallas/MajorMinor/PerChrom/" + MAMIOUT + "_bin-{binNum}.vcf.gz")
        shell:
            """
            if (( $(ulimit -Sn) < {params.nSam} )); then echo "INFO: increasing system ulimit to sample size"; ulimit -n {params.ulim}; fi
            {config[atlas]} majorMinor --glf $(echo {input} | tr [:blank:] ",") --phredLik --out {params.prefix} \
             --sampleNames {params.names} {params.atlParam}  {params.maMiParam}
            """

    rule catMaMi:
        input: 
            mamis=expand((OUTPATH +"/4.Pallas/MajorMinor/PerChrom/"+MAMIOUT+"_bin-{binNum}.vcf.gz"), zip, binNum=binList)
        params:
            prefix=(OUTPATH +"/4.Pallas/MajorMinor/" + MAMIOUT + ".vcf")
        output: 
            mami=(OUTPATH +"/4.Pallas/MajorMinor/" + MAMIOUT + ".vcf.gz")
        resources:
            tmpdir=TMPDIR
        shell:
            """
            bin_array=({input.mamis})
            for i in `seq 0 $(expr ${{#bin_array[@]}} - 1)`; do 
            if [ $i == 0 ]; then
            zcat ${{bin_array[0]}} > {params.prefix}
            else
            zgrep -v "#" ${{bin_array[i]}} >> {params.prefix}
            fi
            done
            gzip {params.prefix}
            """
#########################################################################################################
#########################################################################################################
