if PCANGSD == "install":
    rule pcangsdInstall:
        output: "pcangsd/pcangsd/pcangsd.py"
        params:
            prefix="pcangsd/"
        conda: "../environments/environment_PCAngsd.yaml"
        shell:
            """
            git clone https://github.com/Rosemeis/pcangsd.git pca
            pip3 install -e pca
            mv pca/* pcangsd
            """
'''
elif Path(PCANGSD).is_file():
    rule pcaInstall:
        output: "GATK/GenomeAnalysisTK-3.8-0-ge9d806836/GenomeAnalysisTK.jar"
        params:
            loc=PCANGSD
        conda:
            "../environments/environment_gatk.yaml"
        shell:
            """
            ln -sr {params.loc} {output}
            gatk-register {output}
            """
    inputFiles.append("GATK/GenomeAnalysisTK-3.8-0-ge9d806836/GenomeAnalysisTK.jar")
elif PCANGSD.startswith("module "):
    GATKMOD = PCANGSD.split(' ', 1)

'''