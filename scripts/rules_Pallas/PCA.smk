#########################################################################################################
#########################################################################################################
# Author           : Ilektra Schulz
#                    ilektra.schulz@unifr.ch
# Source           : https://bitbucket.org/wegmannlab/atlas-pipeline/
#########################################################################################################
#########################################################################################################
rule Beagle:
    input:
        mami=(OUTPATH +"/4.Pallas/MajorMinor/" + MAMIOUT + ".vcf.gz")
    params:
        prefix=(OUTPATH +"/4.Pallas/PCA/"+MAMIOUT),
        atlParam=ATLASPARAMS
    conda:
        "../environments/environment_gcc.yaml"
    output:
        (OUTPATH +"/4.Pallas/PCA/"+MAMIOUT+".beagle.gz")
    shell:
        "{config[atlas]} convertVCF --vcf {input} --format beagle  --out {params.prefix}  {params.atlParam}  "

rule PCA:
    input:
        gz=(OUTPATH +"/4.Pallas/PCA/"+MAMIOUT+".beagle.gz")
    output:
        (OUTPATH +"/4.Pallas/PCA/"+MAMIOUT+".cov")
    params:
        prefix=(OUTPATH +"/4.Pallas/PCA/"+MAMIOUT),
        pcangsd=PCANGSD,
        pcaParams=PCAPARAMS
    threads: THREADS
    shell:
        "{params.pcangsd} -b {input} -o {params.prefix} -t {threads} {params.pcaParams}"

rule plotPCA:
    input:
        covFile=(OUTPATH +"/4.Pallas/PCA/"+MAMIOUT+".cov"),
        popFile=SLIST
    output:
        pdf=(OUTPATH +"/4.Pallas/PCA/"+MAMIOUT+".pdf"),
    conda:
        "../environments/environment_R.yaml"
    shell:
        """
        Rscript scripts/rules_Pallas/plot_PCA.R {input.covFile} {input.popFile}
        """

#########################################################################################################
#########################################################################################################
