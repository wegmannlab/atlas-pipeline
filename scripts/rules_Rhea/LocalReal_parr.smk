#########################################################################################################
#########################################################################################################
# Author           : Ilektra Schulz
#                    ilektra.schulz@unifr.ch
# Source           : https://bitbucket.org/wegmannlab/atlas-pipeline/
#########################################################################################################
#########################################################################################################
rule binFiles:
    output:
        binfile=temp((OUTPATH +"/2.Rhea/init/bin_{binNum}-.list"))
    params:
        chrom=lambda wildcards: "\n".join([str(i) for i in binDict[int(wildcards.binNum)]])
    shell:
        """
        echo -e "{params.chrom}" > {output.binfile}
        """

# find private InDel positions per file
rule TargetCreator:
    input:
        bam=lambda wildcards: str(Path(sm["Path"][wildcards.sample])/f"{wildcards.sample}.bam"),
        binFile=(OUTPATH +"/2.Rhea/init/bin_{binNum}-.list"),
        gatkInstallation=inputFiles,
        dict=DICT
    output:
        interval=temp((OUTPATH +"/2.Rhea/Intervals/{sample}-.{binNum}-.intervals")),
    params:
        gatkMod=GATKMOD,
        gatkParam=GATKPARAM,
        known=klist
    threads: THREADS
    conda:
        "../environments/environment_gatk.yaml"
    shell:
        """
        export _JAVA_OPTIONS='-Djava.io.tmpdir={resources.tmpdir}'
        {params.gatkMod}
        gatk -T RealignerTargetCreator \
           -R {config[ref]} -I {input.bam} -L {input.binFile} -o {output} -nt {threads} {params.known} {params.gatkParam}
        """

# only targets:
rule convIntervalsToList:
    input:
        interval=(OUTPATH +"/2.Rhea/Intervals/{target}-.{binNum}-.intervals"),
        header=DICT
    output:
        temp=temp((OUTPATH +"/2.Rhea/Targets/{target}-.{binNum}-.interval_list"))
    shell:
        """
        awk 'BEGIN{{OFS = "\t"}}{{if ($1 == "@SQ") {{print $1,$2,$3}} else {{print}}}}'  {input.header} > {output.temp}
        awk -F ':' 'BEGIN{{OFS = "\t"}} {{if($2 ~ "-"){{split($2,a,"-");print $1,a[1],a[2],"+",$0}} else print $1,$2,$2,"+",$0}}' {input.interval} >> {output.temp}
        """

#only targets: merge private positions of all targets to one interval_list
rule commonTargets:
    input:
        expand(OUTPATH +"/2.Rhea/Targets/{target}-.{{binNum}}-.interval_list", target=tg.index)
    output:
        temp2=(OUTPATH +"/2.Rhea/TargetIntervals/targets-{binNum}.interval_list")
    conda:
        "../environments/environment_picard.yaml"
    shell:
        """
        export _JAVA_OPTIONS='-Xmx120G'
        in=""
        for i in {input}; do this="I=${{i}}"; echo ${{this}}; in2="${{in}} ${{this}}"; in=${{in2}}; done
        set +e
        picard IntervalListTools ACTION=UNION SORT=true UNIQUE=true ${{in}} O={output.temp2}
        exitcode=$?
        if [ $exitcode -eq 1 ]; then exit 1; else exit 0; fi
        """

# unify all private position files with unified target positions
rule unionPrivateAndTargets:
    input:
        inter=(OUTPATH +"/2.Rhea/Intervals/{sample}-.{binNum}-.intervals"),
        IntFile=(OUTPATH +"/2.Rhea/TargetIntervals/targets-{binNum}.interval_list"),
        header=DICT
    output:
        temp=temp((OUTPATH +"/2.Rhea/Intervals/{sample}-.{binNum}-.interval_list")),
        temp1=temp((OUTPATH +"/2.Rhea/GuidanceIntervals/{sample}-.{binNum}-.target.interval_list"))
    conda:
        "../environments/environment_picard.yaml"
    shell:
        """
        awk 'BEGIN{{OFS = "\t"}}{{if ($1 == "@SQ") {{print $1,$2,$3}} else {{print}}}}'  {input.header} > {output.temp}
        awk -F ':' 'BEGIN{{OFS = "\t"}} {{if($2 ~ "-"){{split($2,a,"-");print $1,a[1],a[2],"+",$0}} else print $1,$2,$2,"+",$0}}' {input.inter} >> {output.temp}
        set +e
        export _JAVA_OPTIONS='-Xmx120G'
        picard IntervalListTools ACTION=UNION SORT=true UNIQUE=true I={input.IntFile} I={output.temp} O={output.temp1}
        exitcode=$?
        if [ $exitcode -eq 1 ]; then exit 1; else exit 0; fi
        """

# convert back to intervals file
rule convListToIntervals:
    input:
        temp2=(OUTPATH +"/2.Rhea/GuidanceIntervals/{sample}-.{binNum}-.target.interval_list") #$2
    output:
        temp3=temp((OUTPATH +"/2.Rhea/GuidanceIntervals/{sample}-.{binNum}-.target.intervals"))
    shell:
        """
        set +o pipefail
        awk '$1 !~ /^@/ {{if ($2==$3){{print $1":"$2}} else {{print $1":"$2"-"$3}}}}' {input.temp2} > {output.temp3}
        exitcode=$?
        if [ $exitcode -gt 1 ]; then exit 1; else exit 0; fi
        """

rule makeOutlist:
    output:
        namelist=(OUTPATH +"/2.Rhea/lists/list_{sample}-.{binNum}.map"),
        namelistTmp=(OUTPATH +"/2.Rhea/lists/list_{sample}-.{binNum}.map.tmp"),
        guidanceList=(OUTPATH +"/2.Rhea/lists/list_{sample}-.{binNum}.guidance.list")
    params:
        tmp="/{sample}-.{binNum}/",
        finalpath=(OUTPATH +"/2.Rhea/PerChrom/"),
        finalbam="{sample}-.{binNum}-.bam",
        bamsGuidance=SLIST
    run:
        gu = pd.read_csv(params.bamsGuidance,sep='\t',comment='#')
        guGuidance = gu[(gu.Guidance == "T") & (gu.Sample != wildcards.sample)]
        guOrig = guGuidance.Path[guGuidance.GuidanceAlias == "F"] + guGuidance.Sample[guGuidance.GuidanceAlias == "F"] + ".bam"
        guDs = guGuidance.GuidancePath[gu.GuidanceAlias != "F"] + guGuidance.GuidanceAlias[guGuidance.GuidanceAlias != "F"] + ".bam"
        guOut = pd.concat([guOrig, guDs])
        guOut.to_csv(output.guidanceList,index=False, header=False)
        mapOrig = guGuidance.Sample[guGuidance.GuidanceAlias == "F"] + ".bam " + resources.tmpdir + params.tmp + guGuidance.Sample[guGuidance.GuidanceAlias == "F"] + ".bam"
        mapDs = guGuidance.GuidanceAlias[guGuidance.GuidanceAlias != "F"] + ".bam " + resources.tmpdir + params.tmp + guGuidance.GuidanceAlias[guGuidance.GuidanceAlias != "F"] + ".bam"
        mapSelf = gu.Sample[gu.Sample == wildcards.sample] + ".bam " + params.finalpath + params.finalbam
        mapOut = pd.concat([mapSelf, mapOrig, mapDs])
        mapOut.to_csv(output.namelistTmp, index=False, header=False)
        shell(" tr ' ' '\t' <  {output.namelistTmp} > {output.namelist} ")

rule LocalRealignment:
    input:
        bam=lambda wildcards: str(Path(sm["Path"][wildcards.sample])/f"{wildcards.sample}.bam"),
        guidanceList=(OUTPATH +"/2.Rhea/lists/list_{sample}-.{binNum}.guidance.list"),
        intervals=(OUTPATH +"/2.Rhea/GuidanceIntervals/{sample}-.{binNum}-.target.intervals"),
        paths=(OUTPATH +"/2.Rhea/lists/list_{sample}-.{binNum}.map"),
        binFile=(OUTPATH +"/2.Rhea/init/bin_{binNum}-.list"),
        placeholder=(OUTPATH +"/2.Rhea/tmp/{sample}-.{binNum}/{sample}-{binNum}-.txt")
    output:
        out=temp((OUTPATH +"/2.Rhea/PerChrom/{sample}-.{binNum}-.bam")),
        bai=temp((OUTPATH +"/2.Rhea/PerChrom/{sample}-.{binNum}-.bai"))
    params:
        tmp="/{sample}-.{binNum}/",
        gatkMod=GATKMOD,
        gatkParam=GATKPARAM,
        known=klist
    threads: THREADS
    conda:
        "../environments/environment_gatk.yaml"
    shell:
        """
        export _JAVA_OPTIONS='-Xmx120G -Djava.io.tmpdir={resources.tmpdir}'
        if [ -s {input.guidanceList} ]; then mkdir -p {resources.tmpdir}{params.tmp}; gu=$(echo "-I {input.guidanceList}"); else gu="";fi
       {params.gatkMod}
        gatk -T IndelRealigner -R {config[ref]} -I {input.bam} $gu -targetIntervals {input.intervals} -L {input.binFile} \
         {params.known} -nt {threads} -nWayOut {input.paths} {params.gatkParam}
        echo "deleting temporary files..."
        rm -rf {resources.tmpdir}/{params.tmp}
        """

rule mergeBinsRhea:
    input:
        bams=expand(OUTPATH +"/2.Rhea/PerChrom/{{sample}}-.{binNum}-.bam", binNum=binList)
    output:
        bam=(OUTPATH +"/2.Rhea/final/{sample}.bam"),
        bai=(OUTPATH +"/2.Rhea/final/{sample}.bam.bai")
    conda:
        "../environments/environment_samtools.yaml"
    shell:
        """
        samtools merge -c {output.bam} {input.bams}
        samtools index {output.bam}
        """
#########################################################################################################
#########################################################################################################
