
#########################################################################################################
#-----------------------------------------------RHEA----------------------------------------------------#
#########################################################################################################
# Author           : Ilektra Schulz
#                    ilektra.schulz@unifr.ch
# Source           : https://bitbucket.org/wegmannlab/atlas-pipeline/
#########################################################################################################
#########################################################################################################
#load samplelist as dataframe
if config['sampleFile'] == "fromGaia":
    SLIST = (OUTPATH +"/1.Gaia/outfiles/Gaia_outTable.tsv")
else:
    SLIST = config['sampleFile']

#load data tables
sm = pd.read_csv(SLIST, sep='\t', index_col='Sample', comment='#')
df = sm[sm.LocalReal == "T"]
tg = sm[sm.Target == "T"]

#defaults
GATKMOD = ""
GATKPARAM = config.get("gatkParams","")
GATKINIT = config.get("gatkInit","install")
PARA = config.get("parallelize","F")
BINCOUNT = config.get("binCount","F")
CHRPAR = config.get("chromPar","all")
THREADS = config.get("threads",8)
KNOWN = config.get("known","F")

# get known InDel sites as list
if KNOWN != "F":
    klist = ["--known " + item for item in KNOWN.split(",")]
else:
    klist = []

# get header file from dict
DICT = (os.path.splitext(config['ref'])[0] + ".dict")

inputFiles = []
include: "init.smk"
include: "../rules_general/contigs.smk"

# include
if PARA == "T":
    include: "LocalReal_parr.smk"
else:
    include: "LocalReal_noParr.smk"


#make inputfile for Rhea, Perses or Pallas
def writeOutTable():
    outTable = sm.reset_index(level=0)[['Sample']]
    outTable['Path'] = (OUTPATH +"/2.Rhea/final/")
    outTable['poolRecal'] = "F"
    outTable['poolPMD'] = "F"
    outTable.to_csv((OUTPATH +'/2.Rhea/outfiles/Rhea_outTable.tsv'),sep="\t",index=False)


rule outTable:
    input: expand((OUTPATH +"/2.Rhea/final/{sample}.bam"),zip,sample=df.index)
    output: (OUTPATH +'/2.Rhea/outfiles/Rhea_outTable.tsv')
    run:
        writeOutTable()
#########################################################################################################
#########################################################################################################
