#########################################################################################################
#########################################################################################################
# Author           : Ilektra Schulz
#                    ilektra.schulz@unifr.ch
# Source           : https://bitbucket.org/wegmannlab/atlas-pipeline/
#########################################################################################################
#########################################################################################################

# find private InDel positions per file
rule TargetCreator:
    input:
        bam=lambda wildcards: str(Path(sm["Path"][wildcards.sample])/f"{wildcards.sample}.bam"),
        gatkInstallation=inputFiles,
        dict=DICT
    output:
        interval=(OUTPATH +"/2.Rhea/Intervals/{sample}.intervals")
    conda:
        "../environments/environment_gatk.yaml"
    params:
        gatkMod=GATKMOD,
        gatkParam=GATKPARAM,
        known=klist
    threads: THREADS
    shell:
        """
        export _JAVA_OPTIONS='-Djava.io.tmpdir={resources.tmpdir}'
        {params.gatkMod}
        gatk \
           -T RealignerTargetCreator -R {config[ref]} -Djava.io.tmpdir=params.tmpDir -I {input.bam} -o {output} \
           -nt {threads} {params.known} {params.gatkParam}
        """

# only targets:
rule convIntervalsToList:
        input:
            interval=(OUTPATH +"/2.Rhea/Intervals/{target}.intervals"),
            dict=DICT
        output:
            temp=(OUTPATH +"/2.Rhea/Targets/{target}.interval_list")
        shell:
            """
            awk 'BEGIN{{OFS = "\t"}}{{if ($1 == "@SQ") {{print $1,$2,$3}} else {{print}}}}'  {input.dict} > {output.temp}
            awk -F ':' 'BEGIN{{OFS = "\t"}} {{if($2 ~ "-"){{split($2,a,"-");print $1,a[1],a[2],"+",$0}} else print $1,$2,$2,"+",$0}}' {input.interval} >> {output.temp}
            """

#only targets: merge private positions of all targets to one interval_list
rule commonTargets:
    input:
        expand((OUTPATH +"/2.Rhea/Targets/{target}.interval_list"), target=tg.index) #$1
    output:
        temp2=(OUTPATH +"/2.Rhea/TargetIntervals/targets.interval_list")
    conda:
        "../environments/environment_picard.yaml"
    shell:
        """
        export _JAVA_OPTIONS='-Xmx120G'
        in=
        for i in {input}; do this="I=${{i}}"; in2="${{in}} ${{this}}" in=${{in2}};  done
        set +e
        picard IntervalListTools ACTION=UNION SORT=true UNIQUE=true ${{in}} O={output.temp2}
        exitcode=$?
        if [ $exitcode -eq 1 ]; then exit 1; else exit 0; fi
        """

# unify all private position files with unified target positions
rule unionPrivateAndTargets:
    input:
        sam_int=(OUTPATH +"/2.Rhea/Intervals/{sample}.intervals"), #$1
        target_int=(OUTPATH +'/2.Rhea/TargetIntervals/targets.interval_list'),
        dict=DICT
    output:
        temp=temp((OUTPATH +"/2.Rhea/Intervals/{sample}.interval_list")),
        temp2=temp((OUTPATH +"/2.Rhea/GuidanceIntervals/{sample}.target.interval_list"))
    conda:
        "../environments/environment_picard.yaml"
    shell:
        """
        awk 'BEGIN{{OFS = "\t"}}{{if ($1 == "@SQ") {{print $1,$2,$3}} else {{print}}}}'  {input.dict} > {output.temp}
        awk -F ':' 'BEGIN{{OFS = "\t"}} {{if($2 ~ "-"){{split($2,a,"-");print $1,a[1],a[2],"+",$0}} else print $1,$2,$2,"+",$0}}' {input.sam_int} >> {output.temp}
        set +e
        export _JAVA_OPTIONS='-Xmx120G'
        picard IntervalListTools ACTION=UNION  SORT=true UNIQUE=true I={input.target_int} I={output.temp} O={output.temp2}
        exitcode=$?
        if [ $exitcode -eq 1 ]; then exit 1; else exit 0; fi
        """

# convert back to intervals file
rule convListToIntervals:
    input:
        temp2=(OUTPATH +"/2.Rhea/GuidanceIntervals/{sample}.target.interval_list") #$2
    output:
        temp=temp((OUTPATH +"/2.Rhea/GuidanceIntervals/{sample}.target.intervals"))
    shell:
        """
        set +o pipefail
        awk '$1 !~ /^@/ {{if ($2==$3){{print $1":"$2}} else {{print $1":"$2"-"$3}}}}' {input.temp2} > {output.temp}
        exitcode=$?
        if [ $exitcode -gt 1 ]; then exit 1; else exit 0; fi
        """

rule makeOutlist:
    output:
        namelist=(OUTPATH +"/2.Rhea/lists/list_{sample}.map"),
        namelistTmp=(OUTPATH +"/2.Rhea/lists/list_{sample}.map.tmp"),
        guidanceList=(OUTPATH +"/2.Rhea/lists/list_{sample}.guidance.list")
    params:
        finalpath=(OUTPATH +"/2.Rhea/final/"),
        tmpDir="/{sample}/",
        finalbam="{sample}.bam",
        bamsGuidance=SLIST
    run:
        gu = pd.read_csv(params.bamsGuidance,sep='\t',comment='#')
        guGuidance = gu[(gu.Guidance == "T") & (gu.Sample != wildcards.sample)]
        guOrig = guGuidance.Path[guGuidance.GuidanceAlias == "F"] + guGuidance.Sample[guGuidance.GuidanceAlias == "F"] + ".bam"
        guDs = guGuidance.GuidancePath[gu.GuidanceAlias != "F"] + guGuidance.GuidanceAlias[guGuidance.GuidanceAlias != "F"] + ".bam"
        guOut = pd.concat([guOrig, guDs])
        guOut.to_csv(output.guidanceList,index=False,header=False)
        mapOrig = guGuidance.Sample[guGuidance.GuidanceAlias == "F"] + ".bam " + resources.tmpdir + params.tmpDir + guGuidance.Sample[guGuidance.GuidanceAlias == "F"] + ".bam"
        mapDs = guGuidance.GuidanceAlias[guGuidance.GuidanceAlias != "F"] + ".bam " + resources.tmpdir + params.tmpDir +  guGuidance.GuidanceAlias[guGuidance.GuidanceAlias != "F"] + ".bam"
        mapSelf = gu.Sample[gu.Sample == wildcards.sample] + ".bam " + params.finalpath + params.finalbam
        mapOut = pd.concat([mapSelf, mapOrig, mapDs])
        mapOut.to_csv(output.namelistTmp, index=False, header=False)
        shell(" tr ' ' '\t' <  {output.namelistTmp} > {output.namelist} ")

rule LocalRealignment:
    input:
        bam=lambda wildcards: str(Path(sm["Path"][wildcards.sample])/f"{wildcards.sample}.bam"),
        guidanceList=(OUTPATH +"/2.Rhea/lists/list_{sample}.guidance.list"),
        intervals=(OUTPATH +"/2.Rhea/GuidanceIntervals/{sample}.target.intervals"),
        paths=(OUTPATH +"/2.Rhea/lists/list_{sample}.map")
    output:
        out=(OUTPATH +"/2.Rhea/final/{sample}.bam"),
        bai=(OUTPATH +"/2.Rhea/final/{sample}.bam.bai")
    params:
        prefix=(OUTPATH +"/2.Rhea/final/{sample}"),
        gatkMod=GATKMOD,
        gatkParam=GATKPARAM,
        known=klist
    threads: THREADS
    conda:
        "../environments/environment_gatk.yaml"
    shell:
        """
        export _JAVA_OPTIONS='-Xmx120G -Djava.io.tmpdir={resources.tmpdir}'
        if [ -s {input.guidanceList} ]; then gu=$(echo "-I {input.guidanceList}"); mkdir -p {resources.tmpdir}/{wildcards.sample}; else gu="";fi
	{params.gatkMod}
        export _JAVA_OPTIONS='-Xmx200G'
	gatk -T IndelRealigner -R {config[ref]} -Djava.io.tmpdir=params.tmpDir -I {input.bam} $gu -targetIntervals {input.intervals} \
            {params.known} -nWayOut {input.paths} {params.gatkParam}
        echo "removing temporary files"
        rm -rf {resources.tmpdir}/{wildcards.sample}
        mv {params.prefix}.bai {output.bai}
        """
#########################################################################################################
#########################################################################################################

