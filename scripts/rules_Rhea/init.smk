if GATKINIT == "install":
    rule gatkInstall:
        output: "GATK/GenomeAnalysisTK-3.8-1-0-gf15c1c3ef/GenomeAnalysisTK.jar"
        params:
            prefix="GATK/",
            file="GenomeAnalysisTK-3.8-1-0-gf15c1c3ef"
        conda:
            "../environments/environment_gatk.yaml"
        shell:
            """
            rm -f {params.prefix}{params.file}.tar.bz2
            wget https://storage.googleapis.com/gatk-software/package-archive/gatk/{params.file}.tar.bz2 -P {params.prefix}
            tar -xvf {params.prefix}{params.file}.tar.bz2 -C {params.prefix}
            gatk-register {params.prefix}{params.file}/GenomeAnalysisTK.jar
            """
    inputFiles.append("GATK/GenomeAnalysisTK-3.8-1-0-gf15c1c3ef/GenomeAnalysisTK.jar")
elif Path(GATKINIT).is_file():
    rule gatkInstall:
        output: "GATK/GenomeAnalysisTK.jar"
        params:
            loc=GATKINIT
        conda:
            "../environments/environment_gatk.yaml"
        shell:
            """
            ln -sr {params.loc} {output}
            gatk-register {output}
            """
    inputFiles.append("GATK/GenomeAnalysisTK.jar")
elif GATKINIT.startswith("module "):
    GATKMOD = GATKINIT.split(' ', 1)

if not Path(DICT).is_file():
    rule makeDict:
        input: config['ref']
        output: DICT
        conda:
            "../environments/environment_picard.yaml"
        shell:
            "picard CreateSequenceDictionary R={config[ref]} O={output}"