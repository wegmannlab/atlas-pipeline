#########################################################################################################
#########################################################################################################
# Author           : Ilektra Schulz
#                    ilektra.schulz@unifr.ch
# Source           : https://bitbucket.org/wegmannlab/atlas-pipeline/
#########################################################################################################
#########################################################################################################
import pandas as pd
from pathlib import Path
import os
import yaml
import tempfile

#general defaults and variables
TMPDIR = config.get("tmpDir",tempfile.gettempdir())
RUN = config['runScript']
OUTPATH = config.get("outPath","results")

#print all workflow objects:
#print(vars(workflow))

#get environment file from config file if envmodules to be used
if workflow.use_env_modules:
    with open(config['envmodules'], 'r') as stream:
        envDict = yaml.safe_load(stream)

if RUN == "Gaia":
    localrules: all, file_summary, FileCounts, SamCounts, File_COUNT, SamCOUNT, outTable
    include: "scripts/rules_Gaia/Gaia_Main.smk"
    rule all:
        input:
            (OUTPATH + "/1.Gaia/outfiles/FILE_COUNTS"),
            (OUTPATH + "/1.Gaia/outfiles/SAM_COUNTS"),
            (OUTPATH + "/1.Gaia/outfiles/Gaia_outTable.tsv"),
            expand((OUTPATH + "/1.Gaia/summaries/fil/{file}.summary.txt"), zip, file=df.index.get_level_values(0))

if RUN == "Rhea":
    localrules: all, gatkInstall, binFiles, convIntervalsToList, convListToIntervals, makeOutlist
    include: "scripts/rules_Rhea/Rhea_Main.smk"
    rule all:
        input: (OUTPATH + '/2.Rhea/outfiles/Rhea_outTable.tsv')

if RUN == "Perses":
    localrules: all, summary, PMD_inputFile, recal_inputFile
    include: "scripts/rules_Perses/Perses_Main.smk"
    rule all:
        input: (OUTPATH + '/3.Perses/outfiles/Perses_outTable.tsv')

if RUN == "Pallas":
    localrules: all, summary, getGlobalTheta
    include: "scripts/rules_Pallas/Pallas_Main.smk"
    rule all:
        input:
            expand((OUTPATH + "/4.Pallas/summaries/{sample}.summary.txt"), zip, sample=df.index),
            finalInputFiles

if RUN == "Tests":
    include: "scripts/rules_general/createTestset.smk"
    rule all:
        input: (OUTPATH + "/0.Testset/ATLAS_simulations.fasta.bwt")
#########################################################################################################
#########################################################################################################

